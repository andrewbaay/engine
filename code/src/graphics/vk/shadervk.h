#pragma once
#include <string>
#include <string_view>
#include <vulkan/vulkan.h>

#include "vertexvk.h"
#include "viewvk.h"

class CShaderSystemVK;
class CMaterialInstanceVK;
class CSceneVK;
class CMeshVK;

class CShaderVK
{
public:
	virtual void InitShader() = 0;
	virtual void InitMaterialInstance(CSceneVK *scene, CMaterialInstanceVK *instance) = 0;

	virtual void UpdateBuffers(CMeshVK *mesh, CViewVK *view, uint32_t frame) = 0;

	virtual std::vector<VkDescriptorSetLayoutBinding> GetDescriptorSetLayoutBindings() const = 0;

	virtual void CreateGraphicsPipeline() = 0;

	void CreateDescriptorSetLayout();
	void CreateGraphicsPipelineLayout();
	
	CShaderSystemVK *shaderSystem = nullptr;
	VkDescriptorSetLayout descriptorSetLayout = VK_NULL_HANDLE;
	VkPipelineLayout pipelineLayout = VK_NULL_HANDLE;
	VkPipeline pipeline = VK_NULL_HANDLE;


	std::string_view shaderName;
};