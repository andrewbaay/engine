#pragma once

#include "renderer.h"
#include "renderwindow.h"
#include "scenevk.h"
#include "shadersystem.h"

#include <vulkan/vulkan.h>
#include <vector>

class CGraphicsVK;

class CRendererVK : public IRenderer
{
public:

	IScene *CreateScene() override;
	void DeleteScene(IScene *baseScene) override;

	IWindow *GetWindow() override { return renderWindow; }

	void SetClearColor(float red, float green, float blue, float alpha) { clearColorValue = { red, green, blue, alpha }; }

	void BeginFrame();
	void DrawView(IView *baseView) override;
	void EndFrame();

	CGraphicsVK *graphics = nullptr;
	CRenderWindow *renderWindow = nullptr;

	std::vector<VkCommandBuffer> commandBuffers;
	uint32_t imageIndex = 0;
	uint32_t currentFrame = 0;

	std::unique_ptr<CShaderSystemVK> shaderSystem;
	std::vector<std::unique_ptr<CSceneVK>> scenes;

	VkClearColorValue clearColorValue = { 0.0f, 0.0f, 0.0f, 1.0f };

	bool isReadyToDraw = false;
};