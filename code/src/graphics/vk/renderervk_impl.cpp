#include "renderervk_impl.h"
#include "log.h"
#include "graphicsvk.h"

#include <array>

IScene *CRendererVK::CreateScene()
{
	scenes.emplace_back(std::make_unique<CSceneVK>());

	CSceneVK *scene = scenes.back().get();
	scene->renderer = this;
	scene->LoadDefaultResources();

	return scene;
}

void CRendererVK::DeleteScene(IScene *baseScene)
{
	auto scene_it = std::find_if(scenes.begin(), scenes.end(), [ baseScene ](const std::unique_ptr<CSceneVK> &scene){ return baseScene == scene.get(); });

	if (scene_it != scenes.end())
	{
		vkDeviceWaitIdle(graphics->device);

		CSceneVK *scene = scene_it->get();
		scene->UnloadModels();
		scene->UnloadTextures();
		scenes.erase(scene_it);
	}
}

void CRendererVK::BeginFrame()
{
	vkWaitForFences(graphics->device, 1, &renderWindow->inFlightFences[ currentFrame ], VK_TRUE, std::numeric_limits< uint64_t >::max());

	if (vkAcquireNextImageKHR(graphics->device, renderWindow->swapChainKHR, std::numeric_limits<uint64_t>::max(), renderWindow->imageAvailableSemaphores[ currentFrame ], VK_NULL_HANDLE, &imageIndex) != VK_SUCCESS)
	{
		Log::Print(ESeverity::Warning, "Failed to acquire swap chain image\n");
		return;
	}

	auto &commandBuffer = commandBuffers[ imageIndex ];

	VkCommandBufferBeginInfo beginInfo = {};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
	beginInfo.pInheritanceInfo = nullptr; // Optional

	if (vkBeginCommandBuffer(commandBuffer, &beginInfo) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create command buffers");

	VkRenderPassBeginInfo renderPassInfo = {};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	renderPassInfo.renderPass = renderWindow->renderPass;
	renderPassInfo.framebuffer = renderWindow->swapChainFramebuffers[ imageIndex ];
	renderPassInfo.renderArea.offset = { 0, 0 };
	renderPassInfo.renderArea.extent = renderWindow->swapChainExtent;

	std::array<VkClearValue, 2> clearValues;
	clearValues[0].color = clearColorValue;
	clearValues[1].depthStencil = { 1.0f, 0 };

	renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
	renderPassInfo.pClearValues = clearValues.data();

	vkCmdBeginRenderPass(commandBuffer, &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
	isReadyToDraw = true;
}

void CRendererVK::DrawView(IView *baseView)
{
	if (isReadyToDraw)
	{
		CViewVK *view = CViewVK::ToViewVK(baseView);

		// ACTUAL DRAWING CODE HERE
		auto &commandBuffer = commandBuffers[ imageIndex ];
		CSceneVK *scene = view->scene;
	
		CModelVK *cube = scene->cube;
		for (auto &mesh : cube->meshes)
		{
			CShaderVK *shader = mesh->materialInstance->material->GetShader();
			shader->UpdateBuffers(mesh.get(), view, currentFrame);

			vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, shader->pipeline);
			vkCmdBindDescriptorSets(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, shader->pipelineLayout, 0, 1, &mesh->materialInstance->descriptorSets[currentFrame], 0, nullptr);

			const VkDeviceSize offset = 0;
			vkCmdBindVertexBuffers(commandBuffer, 0, 1, &mesh->vertexBuffer, &offset);

			if (mesh->indexBuffer != VK_NULL_HANDLE)
			{
				vkCmdBindIndexBuffer(commandBuffer, mesh->indexBuffer, 0, VK_INDEX_TYPE_UINT32);
				vkCmdDrawIndexed(commandBuffer, mesh->indexCount, 1, 0, 0, 0);
			}
			else
				vkCmdDraw(commandBuffer, mesh->vertexCount, 1, 0, 0);
		}
	}
}

void CRendererVK::EndFrame()
{
	if (isReadyToDraw)
	{
		auto &commandBuffer = commandBuffers[imageIndex];

		vkCmdEndRenderPass(commandBuffer);

		if (vkEndCommandBuffer(commandBuffer) != VK_SUCCESS)
			throw std::runtime_error("[Vulkan]Failed to end command buffer");

		VkSemaphore waitSemaphores[] = { renderWindow->imageAvailableSemaphores[ currentFrame ] };
		VkSubmitInfo submitInfo = {};
		submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

		VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
		submitInfo.waitSemaphoreCount = 1;
		submitInfo.pWaitSemaphores = waitSemaphores;
		submitInfo.pWaitDstStageMask = waitStages;
		submitInfo.commandBufferCount = 1;
		submitInfo.pCommandBuffers = &commandBuffers[ imageIndex ];

		VkSemaphore signalSemaphores[] = { renderWindow->renderFinishedSemaphores[ currentFrame ] };
		submitInfo.signalSemaphoreCount = 1;
		submitInfo.pSignalSemaphores = signalSemaphores;

		vkResetFences(graphics->device, 1, &renderWindow->inFlightFences[ currentFrame ]);

		if (vkQueueSubmit(graphics->graphicsQueue, 1, &submitInfo, renderWindow->inFlightFences[ currentFrame ]) != VK_SUCCESS)
		{
			Log::Print(ESeverity::Warning, "Queue submit failed!\n");
			return;
		}

		VkPresentInfoKHR presentInfo = {};
		presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
		presentInfo.waitSemaphoreCount = 1;
		presentInfo.pWaitSemaphores = signalSemaphores;
		presentInfo.swapchainCount = 1;
		presentInfo.pSwapchains = &renderWindow->swapChainKHR;
		presentInfo.pImageIndices = &imageIndex;
		presentInfo.pResults = nullptr; // Optional

		if (vkQueuePresentKHR(graphics->presentQueue, &presentInfo) != VK_SUCCESS)
			Log::Print(ESeverity::Warning, "Queue present failed!\n");
		else
		{
			currentFrame = ( currentFrame + 1 ) % MAX_FRAMES_IN_FLIGHT;
			vkWaitForFences(graphics->device, 1, &renderWindow->inFlightFences[ currentFrame ], VK_TRUE, std::numeric_limits< uint64_t >::max());
		}

		isReadyToDraw = false;
	}
}