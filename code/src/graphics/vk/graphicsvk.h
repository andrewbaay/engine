#pragma once

#include "service.h"
#include "graphics.h"
#include "renderwindow.h"
#include "graphicsvk_definitions.h"
#include "renderervk_impl.h"

#include <SDL2/SDL.h>
#include <vulkan/vulkan.h>
#include <vk_mem_alloc.h>
#include <optional>
#include <memory>

// NOTE: This may require you to actually install/setup the Vulkan SDK on Linux for this to not throw an exception
#define VK_DEBUG 1

class CGraphicsVK : public IGraphics, public CService<>
{
#if VK_DEBUG
	VkResult CreateDebugUtilsMessengerEXT(const VkDebugUtilsMessengerCreateInfoEXT *pCreateInfo, const VkAllocationCallbacks *pAllocator);
	void DestroyDebugUtilsMessengerEXT(VkAllocationCallbacks *pAllocator);
#endif

	bool IsPhysicalDeviceSuitable(CRenderWindow *renderWindow, VkPhysicalDevice device);

public:
	using CService<>::CService;
	~CGraphicsVK();

	void Init(CRenderWindow *renderWindow);
	void Update(std::size_t) override {}

	void OnWindowResize(CRenderWindow *renderWindow);
	void HandleWindowEvent(const SDL_WindowEvent &windowEvent) override;

	IWindow *CreateWindow(const std::string_view &title, int width, int height, bool fullscreen) override;
	void DeleteWindow(IWindow *window) override;

	IRenderer *CreateRenderer(IWindow *window) override;
	void DeleteRenderer(IRenderer *renderer) override;

	void DestroySwapChain(CRenderWindow *renderWindow);

private:

	void LoadExtensions(SDL_Window *window);
	void CheckValidationLayerSupport();
	void CreateInstance();
	void SetupDebugCallback();

	void CreateSurface(CRenderWindow *renderWindow);
	void PickPhysicalDevice(CRenderWindow *renderWindow);
	void CreateLogicalDevice(CRenderWindow *renderWindow);
	void CreateVmaAllocator(CRenderWindow *renderWindow);
	void CreateSwapChain(CRenderWindow *renderWindow);
	void CreateSwapChainImageViews(CRenderWindow *renderWindow);
	void FindDepthFormat(CRenderWindow *renderWindow);
	void CreateRenderPass(CRenderWindow *renderWindow);
	void CreateCommandPool(CRenderWindow *renderWindow);
	void CreateDepthResources(CRenderWindow *renderWindow);
	void CreateFramebuffers(CRenderWindow *renderWindow);
	void CreateSyncObjects(CRenderWindow *renderWindow);

public:

	// Helper functions
	VkCommandBuffer BeginSingleTimeCommands(CRenderWindow *renderWindow);
	void EndSingleTimeCommands(CRenderWindow *renderWindow, VkCommandBuffer &commandBuffer);
	VkImage CreateImage2D(CRenderWindow *renderWindow, uint32_t width, uint32_t height, uint32_t mipLevels, VkFormat format, VkImageTiling tiling, VkImageUsageFlags imageUsage, VmaMemoryUsage memoryUsage, VmaAllocation &allocation);
	VkImageView CreateImageView2D(VkImage image, VkFormat format, VkImageAspectFlags aspectFlags, uint32_t mipLevels);
	void CopyBufferToImage(CRenderWindow *renderWindow, VkBuffer buffer, VkImage image, uint32_t width, uint32_t height);
	void TransitionImageLayout(CRenderWindow *renderWindow, VkImage image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout, uint32_t mipLevels);
	void CreateBuffer(CRenderWindow *renderWindow, VkDeviceSize size, VkBufferUsageFlags bufferUsage, VmaMemoryUsage memoryUsage, VkBuffer &buffer, VmaAllocation &allocation);
	void DeleteBuffer(CRenderWindow *renderWindow, VkBuffer buffer, VmaAllocation allocation);
	void CopyBuffer(CRenderWindow *renderWindow, const VkBuffer &srcBuffer, VkBuffer &dstBuffer, VkDeviceSize size);

	std::vector<const char*> requiredExtensions;
	std::vector<const char*> deviceExtensions = { VK_KHR_SWAPCHAIN_EXTENSION_NAME, VK_KHR_MAINTENANCE1_EXTENSION_NAME };
	std::vector<const char*> validationLayers = { "VK_LAYER_KHRONOS_validation" };

	VkDebugUtilsMessengerEXT debugCallback = VK_NULL_HANDLE;
	VkInstance instance = VK_NULL_HANDLE;
	VkDevice device = VK_NULL_HANDLE;
	VkQueue graphicsQueue = VK_NULL_HANDLE;
	VkQueue presentQueue = VK_NULL_HANDLE;

private:

	// SDL at the moment REQUIRES a window for us to get the required Vulkan instance extensions
	bool isInstanceCreated = false;

	// We need a surface (and therefore a window) to properly create a device, but we only need ONE
	bool isDeviceCreated = false;

	std::unordered_map<uint32_t, std::unique_ptr<CRenderWindow>> renderWindows;
	std::vector<std::unique_ptr<CRendererVK>> renderers;
};