#pragma once

#include "graphicsvk_definitions.h"
#include "graphics.h"
#include "queuefamily.h"

#include <vulkan/vulkan.h>
#include <vk_mem_alloc.h>
#include <memory>

class CRenderWindow : public IWindow
{
public:
	static CRenderWindow *ToRenderWindow(IWindow *window)
	{
#if _DEBUG
		return dynamic_cast<CRenderWindow*>(window);
#else
		return static_cast<CRenderWindow*>(window);
#endif
	}

	float GetAspectRatio() const { return aspectRatio; }
	void RegisterOnClosed(std::function<void(IWindow *)> function) override { onClosedCallbacks.push_back(function); };

	SDL_Window *windowHandle = nullptr;
	uint32_t windowID = {};

	VkSurfaceKHR surface = VK_NULL_HANDLE;
	VkPhysicalDevice physicalDevice = VK_NULL_HANDLE;
	VmaAllocator allocator = VK_NULL_HANDLE;

	// Swap Chain
	VkSwapchainKHR swapChainKHR = VK_NULL_HANDLE;
	std::unique_ptr<VkImage[]> swapChainImages;
	std::unique_ptr<VkImageView[]> swapChainImageViews;
	std::unique_ptr<VkFramebuffer[]> swapChainFramebuffers;
	VkExtent2D swapChainExtent = {};
	VkFormat swapChainImageFormat = VK_FORMAT_UNDEFINED;
	VkFormat depthFormat = VK_FORMAT_UNDEFINED;
	uint32_t numSwapChainImages = 0;
	VkRenderPass renderPass = VK_NULL_HANDLE;
	VkCommandPool commandPool = VK_NULL_HANDLE;
	VkImage depthImage = VK_NULL_HANDLE;
	VmaAllocation depthImageAllocation = VK_NULL_HANDLE;
	VkImageView depthImageView = VK_NULL_HANDLE;

	VkSemaphore imageAvailableSemaphores[ MAX_FRAMES_IN_FLIGHT ] = { VK_NULL_HANDLE };
	VkSemaphore renderFinishedSemaphores[ MAX_FRAMES_IN_FLIGHT ] = { VK_NULL_HANDLE };
	VkFence inFlightFences[ MAX_FRAMES_IN_FLIGHT ] = { VK_NULL_HANDLE };

	QueueFamilyIndices queueFamilyIndices;

	std::vector<std::function<void(IWindow *)>> onClosedCallbacks;

	float aspectRatio = 0.0f;
};