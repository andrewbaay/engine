#include "log/log.h"
#include "graphicsvk.h"

extern "C" THR_EXPORT void ModuleInit(CServer::CServerInstance *serverInstance, CServer *parent)
{
    Log::Init(parent->Get<ILogger>(), "GraphicsVK", fmt::color::cornflower_blue);
	serverInstance->MakeService<CGraphicsVK>(parent, "GraphicsVK");
}

extern "C" THR_EXPORT void ModuleShutdown()
{
    Log::Print("Shutting down...\n");
}