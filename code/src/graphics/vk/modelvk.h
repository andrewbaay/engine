#pragma once

#include <vector>
#include <memory>

#include "meshvk.h"

class CModelVK
{
public:
	std::vector<std::unique_ptr<CMeshVK>> meshes;
};