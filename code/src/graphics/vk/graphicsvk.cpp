#define VMA_IMPLEMENTATION // for vk_mem_alloc.h

#include "graphicsvk.h"
#include "renderwindow.h"
#include "log.h"
#include "shadersystem.h"

#include <algorithm>
#include <array>
#include <map>
#include <set>
#include <SDL2/SDL_vulkan.h>

#if VK_DEBUG
static VKAPI_ATTR VkBool32 VKAPI_CALL DebugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT, VkDebugUtilsMessageTypeFlagsEXT, const VkDebugUtilsMessengerCallbackDataEXT *pCallbackData, void *)
{
	Log::Print(ESeverity::Warning, "Validation layer: {}\n", pCallbackData->pMessage);
	return VK_FALSE;
}
#endif // VK_DEBUG

struct SwapChainSupportDetails
{
	VkSurfaceCapabilitiesKHR capabilities = {};
	std::vector<VkSurfaceFormatKHR> formats;
	std::vector<VkPresentModeKHR> presentModes;
};

SwapChainSupportDetails QuerySwapChainSupport(CRenderWindow *renderWindow, VkPhysicalDevice device)
{
	SwapChainSupportDetails details;
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, renderWindow->surface, &details.capabilities);

	uint32_t formatCount = 0;
	vkGetPhysicalDeviceSurfaceFormatsKHR(device, renderWindow->surface, &formatCount, nullptr);

	if (formatCount != 0)
	{
		details.formats.resize(formatCount);
		vkGetPhysicalDeviceSurfaceFormatsKHR(device, renderWindow->surface, &formatCount, details.formats.data());
	}

	uint32_t presentModeCount = 0;
	vkGetPhysicalDeviceSurfacePresentModesKHR(device, renderWindow->surface, &presentModeCount, nullptr);

	if (presentModeCount != 0)
	{
		details.presentModes.resize( presentModeCount );
		vkGetPhysicalDeviceSurfacePresentModesKHR(device, renderWindow->surface, &presentModeCount, details.presentModes.data());
	}

	return details;
}


#if VK_DEBUG
VkResult CGraphicsVK::CreateDebugUtilsMessengerEXT(const VkDebugUtilsMessengerCreateInfoEXT * pCreateInfo, const VkAllocationCallbacks *pAllocator)
{
	auto func = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");

	if (func)
		return func(instance, pCreateInfo, pAllocator, &debugCallback);

	return VK_ERROR_EXTENSION_NOT_PRESENT;
}

void CGraphicsVK::DestroyDebugUtilsMessengerEXT(VkAllocationCallbacks *pAllocator)
{
	auto func = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");

	if (func != nullptr)
		func(instance, debugCallback, pAllocator);
}
#endif // VK_DEBUG

bool CGraphicsVK::IsPhysicalDeviceSuitable(CRenderWindow *renderWindow, VkPhysicalDevice device)
{
	auto checkDeviceExtensionSupport = [ device, this ]() -> bool
	{
		uint32_t extensionCount = 0;
		vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);

		std::vector<VkExtensionProperties> availableExtensions( extensionCount );
		vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, availableExtensions.data());

		std::set<std::string> requiredDeviceExtensionSet(deviceExtensions.begin(), deviceExtensions.end());

		for (const auto &extension : availableExtensions)
			requiredDeviceExtensionSet.erase(extension.extensionName);

		return (requiredDeviceExtensionSet.empty());
	};

	auto checkSwapChainSupport = [ & ](const SwapChainSupportDetails &swapChainSupport)
	{
		return (!swapChainSupport.formats.empty() && !swapChainSupport.presentModes.empty());
	};

	// TODO: Allow custom callbacks for checking device features and extensions


	return (FindQueueFamilies(renderWindow, device).IsComplete() &&
		checkDeviceExtensionSupport() &&
		checkSwapChainSupport(QuerySwapChainSupport(renderWindow, device)));
}

CGraphicsVK::~CGraphicsVK()
{
#if VK_DEBUG
	if (debugCallback != VK_NULL_HANDLE)
	{
		DestroyDebugUtilsMessengerEXT(nullptr);
		debugCallback = VK_NULL_HANDLE;
	}
#endif

	if (device != VK_NULL_HANDLE)
	{
		vkDestroyDevice(device, nullptr);
		device = VK_NULL_HANDLE;

		// These are cleaned up when the device is destroyed
		graphicsQueue = VK_NULL_HANDLE;
		presentQueue = VK_NULL_HANDLE;
	}

	if (instance != VK_NULL_HANDLE)
	{
		vkDestroyInstance(instance, nullptr);
		instance = VK_NULL_HANDLE;
	}

	requiredExtensions.clear();
}

void CGraphicsVK::Init(CRenderWindow *renderWindow)
{
	LoadExtensions(renderWindow->windowHandle);
	CheckValidationLayerSupport();
	CreateInstance();
	SetupDebugCallback();

	isInstanceCreated = true;
}

void CGraphicsVK::OnWindowResize(CRenderWindow *renderWindow)
{
	vkDeviceWaitIdle(device);

	DestroySwapChain(renderWindow);
	CreateSwapChain(renderWindow);
	CreateSwapChainImageViews(renderWindow);
	CreateRenderPass(renderWindow);
	CreateDepthResources(renderWindow);
	CreateFramebuffers(renderWindow);

	// TODO: Maybe improve this?
	for (auto &renderer : renderers)
	{
		if (renderer->renderWindow == renderWindow)
			renderer->shaderSystem->OnWindowResize();
	}
}

void CGraphicsVK::HandleWindowEvent(const SDL_WindowEvent &windowEvent)
{
	auto window_it = renderWindows.find(windowEvent.windowID);
	if (window_it == renderWindows.end())
		return;
			
	CRenderWindow *renderWindow = window_it->second.get();
			
	switch (windowEvent.event)
	{
		case SDL_WINDOWEVENT_CLOSE:
		{
			for (auto &onClosed : renderWindow->onClosedCallbacks)
				onClosed(renderWindow);

			break;
		}
		case SDL_WINDOWEVENT_RESIZED:
		{
			Log::Print("WindowID: {}\n", windowEvent.windowID);
			OnWindowResize(renderWindow);
			break;
		}
	}
}

IWindow *CGraphicsVK::CreateWindow(const std::string_view &title, int width, int height, bool fullscreen)
{
	std::unique_ptr<CRenderWindow> renderWindow = std::make_unique<CRenderWindow>();

	uint32_t windowFlags = SDL_WINDOW_SHOWN | SDL_WINDOW_VULKAN | SDL_WINDOW_RESIZABLE;
	if (fullscreen)
		windowFlags |= SDL_WINDOW_FULLSCREEN;

	IWindow *baseWindow = renderWindow.get();
	renderWindow->windowHandle = SDL_CreateWindow(title.data(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, windowFlags);
	renderWindow->windowID = SDL_GetWindowID(renderWindow->windowHandle);
	
	SDL_SetRelativeMouseMode(SDL_TRUE);

	if (!isInstanceCreated)
		Init(renderWindow.get());

	CreateSurface(renderWindow.get());
	PickPhysicalDevice(renderWindow.get());

	if (!isDeviceCreated)
		CreateLogicalDevice(renderWindow.get());

	CreateVmaAllocator(renderWindow.get());

	CreateSwapChain(renderWindow.get());
	CreateSwapChainImageViews(renderWindow.get());
	FindDepthFormat(renderWindow.get());
	CreateRenderPass(renderWindow.get());
	CreateCommandPool(renderWindow.get());
	CreateDepthResources(renderWindow.get());
	CreateFramebuffers(renderWindow.get());
	CreateSyncObjects(renderWindow.get());

	renderWindows[ renderWindow->windowID ] = std::move(renderWindow);

	return baseWindow;
}

void CGraphicsVK::DeleteWindow(IWindow *window)
{
	CRenderWindow *renderWindow = CRenderWindow::ToRenderWindow(window);

	if (!renderWindow)
		return;

	if (device != VK_NULL_HANDLE)
		vkDeviceWaitIdle(device);

	DestroySwapChain(renderWindow);

	// Destroy sync objects
	for (std::size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i)
	{
		if (renderWindow->renderFinishedSemaphores[ i ] != VK_NULL_HANDLE) 
		{
			vkDestroySemaphore(device, renderWindow->renderFinishedSemaphores[ i ], nullptr);
			renderWindow->renderFinishedSemaphores[ i ] = VK_NULL_HANDLE;
		}

		if (renderWindow->imageAvailableSemaphores[ i ] != VK_NULL_HANDLE)
		{
			vkDestroySemaphore(device, renderWindow->imageAvailableSemaphores[ i ], nullptr);
			renderWindow->imageAvailableSemaphores[ i ] = VK_NULL_HANDLE;
		}

		if (renderWindow->inFlightFences[ i ] != VK_NULL_HANDLE)
		{
			vkDestroyFence(device, renderWindow->inFlightFences[ i ], nullptr);
			renderWindow->inFlightFences[ i ] = VK_NULL_HANDLE;
		}
	}

	if (renderWindow->commandPool != VK_NULL_HANDLE)
	{
		vkDestroyCommandPool(device, renderWindow->commandPool, nullptr);
		renderWindow->commandPool = VK_NULL_HANDLE;
	}

	if (renderWindow->allocator != VK_NULL_HANDLE)
	{
		vmaDestroyAllocator(renderWindow->allocator);
		renderWindow->allocator = VK_NULL_HANDLE;
	}

	if (renderWindow->surface != VK_NULL_HANDLE)
		vkDestroySurfaceKHR(instance, renderWindow->surface, nullptr);

	// Physical devices are cleaned up when VkInstance is destroyed
	renderWindow->physicalDevice = VK_NULL_HANDLE;

	SDL_DestroyWindow(renderWindow->windowHandle);
	renderWindows.erase(renderWindow->windowID);
}

IRenderer *CGraphicsVK::CreateRenderer(IWindow *window)
{
	CRenderWindow *renderWindow = CRenderWindow::ToRenderWindow(window);

	if (!renderWindow)
		throw std::runtime_error("Tried to create renderer for a NULL window");

	renderers.emplace_back(std::make_unique<CRendererVK>());

	CRendererVK *renderer = renderers.back().get();
	renderer->graphics = this;
	renderer->renderWindow = renderWindow;

	renderer->commandBuffers.resize(renderWindow->numSwapChainImages);

	VkCommandBufferAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.commandPool = renderWindow->commandPool;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandBufferCount = renderWindow->numSwapChainImages;

	if (vkAllocateCommandBuffers(device, &allocInfo, renderer->commandBuffers.data()) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to allocate command buffers");

	renderer->shaderSystem = std::make_unique<CShaderSystemVK>();
	renderer->shaderSystem->renderer = renderer;
	renderer->shaderSystem->LoadShaders();

	return renderer;
}

void CGraphicsVK::DeleteRenderer(IRenderer *baseRenderer)
{
	auto renderer_it = std::find_if(renderers.begin(), renderers.end(), [&baseRenderer](const std::unique_ptr<CRendererVK> &renderer){ return (baseRenderer == renderer.get());});

	if (renderer_it != renderers.end())
	{
		CRendererVK *renderer = renderer_it->get();
		CRenderWindow *renderWindow = renderer->renderWindow;
		vkDeviceWaitIdle(device);

		renderer->shaderSystem->UnloadShaders();

		if (renderer->commandBuffers.size() > 0)
		{
			vkFreeCommandBuffers(device, renderWindow->commandPool, static_cast<uint32_t>(renderer->commandBuffers.size()), renderer->commandBuffers.data());
			renderer->commandBuffers.clear();
		}

		renderers.erase(renderer_it);
	}
}

void CGraphicsVK::DestroySwapChain(CRenderWindow *renderWindow)
{
	if (renderWindow->depthImageView != VK_NULL_HANDLE)
	{
		vkDestroyImageView(device, renderWindow->depthImageView, nullptr);
		renderWindow->depthImageView = VK_NULL_HANDLE;
	}

	if (renderWindow->depthImage != VK_NULL_HANDLE)
	{
		vmaDestroyImage(renderWindow->allocator, renderWindow->depthImage, renderWindow->depthImageAllocation);
		renderWindow->depthImage = VK_NULL_HANDLE;
		renderWindow->depthImageAllocation = VK_NULL_HANDLE;
	}

	for (uint32_t i = 0; i < renderWindow->numSwapChainImages; ++i)
	{
		if (renderWindow->swapChainFramebuffers && renderWindow->swapChainFramebuffers[ i ] != VK_NULL_HANDLE)
			vkDestroyFramebuffer(device, renderWindow->swapChainFramebuffers[ i ], nullptr);

		if (renderWindow->swapChainImageViews && renderWindow->swapChainImageViews[ i ] != VK_NULL_HANDLE)
			vkDestroyImageView(device, renderWindow->swapChainImageViews[ i ], nullptr);
	}

	renderWindow->swapChainFramebuffers.reset();
	renderWindow->swapChainImageViews.reset();
	renderWindow->swapChainImages.reset();

	if (renderWindow->renderPass != VK_NULL_HANDLE)
	{
		vkDestroyRenderPass(device, renderWindow->renderPass, nullptr);
		renderWindow->renderPass = VK_NULL_HANDLE;
	}

	if (renderWindow->swapChainKHR != VK_NULL_HANDLE)
	{
		vkDestroySwapchainKHR(device, renderWindow->swapChainKHR, nullptr);
		renderWindow->swapChainKHR = VK_NULL_HANDLE;
	}
}

void CGraphicsVK::LoadExtensions(SDL_Window *window)
{
	unsigned int extensionCount = 0;
	SDL_Vulkan_GetInstanceExtensions(window, &extensionCount, nullptr);

	if (extensionCount == 0)
		throw std::runtime_error("[Vulkan]Failed to load required extensions");

	requiredExtensions.resize(extensionCount);
	SDL_Vulkan_GetInstanceExtensions(window, &extensionCount, requiredExtensions.data());

#if VK_DEBUG
	requiredExtensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
	requiredExtensions.push_back(VK_EXT_VALIDATION_FEATURES_EXTENSION_NAME);
#endif

	Log::Print("Required Extensions:\n");

	for (auto &extension : requiredExtensions)
		Log::Print("\t{}\n", extension);
}

void CGraphicsVK::CheckValidationLayerSupport()
{
#if VK_DEBUG
	uint32_t layerCount = 0;
	vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

	std::vector<VkLayerProperties> availableLayers(static_cast<std::size_t>(layerCount));
	vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

	for (auto layerName : validationLayers)
	{
		bool bLayerFound = false;

		for (const auto &layerProperties : availableLayers)
		{
			if (std::strcmp(layerName, layerProperties.layerName) == 0)
			{
				bLayerFound = true;
				break;
			}
		}

		if (!bLayerFound)
			throw std::runtime_error("[Vulkan]Failed to acquire validation layers");
	}
#endif
}

void CGraphicsVK::CreateInstance()
{	
#if VK_DEBUG
	VkValidationFeaturesEXT validationEXT = {};
	validationEXT.sType = VK_STRUCTURE_TYPE_VALIDATION_FEATURES_EXT;
	validationEXT.enabledValidationFeatureCount = 0;
	validationEXT.pEnabledValidationFeatures = nullptr;
#endif

	VkApplicationInfo appInfo = {};
	appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	appInfo.pApplicationName = "Thermite";
	appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
	appInfo.pEngineName = "Thermite";
	appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
	appInfo.apiVersion = VK_API_VERSION_1_2;

	VkInstanceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	createInfo.pApplicationInfo = &appInfo;
	createInfo.enabledExtensionCount = static_cast<uint32_t>(requiredExtensions.size());
	createInfo.ppEnabledExtensionNames = requiredExtensions.data();

#if VK_DEBUG
	createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
	createInfo.ppEnabledLayerNames = validationLayers.data();
	createInfo.pNext = &validationEXT;
#else // !VK_DEBUG
	createInfo.enabledLayerCount = 0;
#endif // VK_DEBUG

	if (vkCreateInstance(&createInfo, nullptr, &instance) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create instance");

	// Query available extensions
	uint32_t extensionCount = 0;
	vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);

	std::vector< VkExtensionProperties > extensionProperties{ static_cast<size_t>(extensionCount) };
	vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, extensionProperties.data());

	Log::Print( "Available Extensions:\n" );

	for (const auto &extensionProperty : extensionProperties)
		Log::Print("\t{}\n", extensionProperty.extensionName);
}

void CGraphicsVK::SetupDebugCallback()
{
#if VK_DEBUG
	VkDebugUtilsMessengerCreateInfoEXT createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
	createInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
	createInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
	createInfo.pfnUserCallback = DebugCallback;
	createInfo.pUserData = nullptr;

	if (CreateDebugUtilsMessengerEXT( &createInfo, nullptr ) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to setup debug callback");
#endif
}

void CGraphicsVK::CreateSurface(CRenderWindow *renderWindow)
{
	if (SDL_Vulkan_CreateSurface(renderWindow->windowHandle, instance, &renderWindow->surface) != SDL_TRUE)
		throw std::runtime_error("[Vulkan]Failed to create window surface");
}

void CGraphicsVK::PickPhysicalDevice(CRenderWindow *renderWindow)
{
	uint32_t deviceCount = 0;
	vkEnumeratePhysicalDevices(instance, &deviceCount, nullptr);

	if (deviceCount == 0)
		throw std::runtime_error("[Vulkan]Failed to find physical device");

	std::vector<VkPhysicalDevice> devices(static_cast< size_t >(deviceCount));
	vkEnumeratePhysicalDevices(instance, &deviceCount, devices.data());

	// TODO: Allow for supplying custom physical device rating callback
	auto rateSuitability = [](VkPhysicalDevice device)
	{
		uint32_t iScore = 1;

		VkPhysicalDeviceProperties deviceProperties;
		vkGetPhysicalDeviceProperties(device, &deviceProperties);

		VkPhysicalDeviceFeatures deviceFeatures;
		vkGetPhysicalDeviceFeatures(device, &deviceFeatures);

		if (deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
			iScore += 1000;

		iScore += deviceProperties.limits.maxImageDimension2D;

		return iScore;
	};

	// Ordered map that automatically sorts device candidates by increasing score
	std::multimap<uint32_t, VkPhysicalDevice> candidates;

	for (VkPhysicalDevice &device : devices)
	{
		if (!IsPhysicalDeviceSuitable(renderWindow, device))
			continue;

		uint32_t score = rateSuitability(device);
		candidates.insert(std::make_pair(score, device));
	}

	if (!candidates.empty() && candidates.rbegin()->first > 0)
		renderWindow->physicalDevice = candidates.rbegin()->second;

	if (!renderWindow->physicalDevice)
		throw std::runtime_error("[Vulkan]Failed to find physical device");

	renderWindow->queueFamilyIndices = FindQueueFamilies(renderWindow, renderWindow->physicalDevice);
}

void CGraphicsVK::CreateLogicalDevice(CRenderWindow *renderWindow)
{
	std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;

	std::set<uint32_t> uniqueQueueFamilies =
	{
		renderWindow->queueFamilyIndices.graphicsFamily.value(),
		renderWindow->queueFamilyIndices.presentFamily.value()
	};

	const float queuePriority = 1.0f;
	for (auto &queueFamily : uniqueQueueFamilies)
	{
		VkDeviceQueueCreateInfo queueCreateInfo = {};
		queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueCreateInfo.queueFamilyIndex = queueFamily;
		queueCreateInfo.queueCount = 1;
		queueCreateInfo.pQueuePriorities = &queuePriority;

		queueCreateInfos.push_back(queueCreateInfo);
	}

	// TODO: Revisit this in the future for requesting device features
	VkPhysicalDeviceFeatures deviceFeatures = {};
	vkGetPhysicalDeviceFeatures(renderWindow->physicalDevice, &deviceFeatures);

	VkDeviceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	createInfo.queueCreateInfoCount = static_cast<uint32_t>(queueCreateInfos.size());
	createInfo.pQueueCreateInfos = queueCreateInfos.data();
	createInfo.pEnabledFeatures = &deviceFeatures;

	createInfo.enabledExtensionCount = static_cast<uint32_t>(deviceExtensions.size());
	createInfo.ppEnabledExtensionNames = deviceExtensions.data();

#if VK_DEBUG
	createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
	createInfo.ppEnabledLayerNames = validationLayers.data();
#else // !VK_DEBUG
	createInfo.enabledLayerCount = 0;
#endif // VK_DEBUG

	if (vkCreateDevice(renderWindow->physicalDevice, &createInfo, nullptr, &device) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create logical device");

	vkGetDeviceQueue(device, renderWindow->queueFamilyIndices.graphicsFamily.value(), 0, &graphicsQueue);
	vkGetDeviceQueue(device, renderWindow->queueFamilyIndices.presentFamily.value(), 0, &presentQueue);

	isDeviceCreated = true;
}

void CGraphicsVK::CreateVmaAllocator(CRenderWindow *renderWindow)
{
	VmaAllocatorCreateInfo allocatorInfo = {};
	allocatorInfo.physicalDevice = renderWindow->physicalDevice;
	allocatorInfo.device = device;

	vmaCreateAllocator(&allocatorInfo, &renderWindow->allocator);
}

void CGraphicsVK::CreateSwapChain(CRenderWindow *renderWindow)
{
	int width = 0;
	int height = 0;

	SDL_GetWindowSize(renderWindow->windowHandle, &width, &height);

	auto chooseSwapSurfaceFormat = [](const std::vector< VkSurfaceFormatKHR > &availableFormats)
	{
		if (availableFormats.size() == 1 && availableFormats[ 0 ].format == VK_FORMAT_UNDEFINED)
			return VkSurfaceFormatKHR{ VK_FORMAT_B8G8R8A8_UNORM, VK_COLOR_SPACE_SRGB_NONLINEAR_KHR };

		for (const auto &format : availableFormats)
		{
			if (format.format == VK_FORMAT_B8G8R8A8_UNORM && format.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
				return format;
		}

		return availableFormats[ 0 ];
	};

	// TODO: Revisit this in the future for VSync options
	auto chooseSwapPresentMode = [](const std::vector< VkPresentModeKHR > &availablePresentModes)
	{
		auto selectedMode = VK_PRESENT_MODE_FIFO_KHR;

		for (const auto &presentMode : availablePresentModes)
		{
			if (presentMode == VK_PRESENT_MODE_MAILBOX_KHR)
				return presentMode;
			else if (presentMode == VK_PRESENT_MODE_IMMEDIATE_KHR)
				selectedMode = presentMode;
		}

		return selectedMode;
	};

	auto chooseSwapExtent = [ &width, &height, this ](const VkSurfaceCapabilitiesKHR &capabilities)
	{
		if (width == 0 || height == 0)
			throw std::runtime_error("[Vulkan] Window created with size of 0!");

		VkExtent2D actualExtent = { static_cast<uint32_t>(width), static_cast<uint32_t>(height) };

		actualExtent.width = std::clamp(actualExtent.width, capabilities.minImageExtent.width, capabilities.maxImageExtent.width);
		actualExtent.height = std::clamp(actualExtent.height, capabilities.minImageExtent.height, capabilities.maxImageExtent.height);

		if (actualExtent.width == 0 || actualExtent.height == 0)
			throw std::runtime_error("[Vulkan]Swap chain capabilities clamped extent to 0!");

		return actualExtent;
	};

	auto swapChainSupport = QuerySwapChainSupport(renderWindow, renderWindow->physicalDevice);

	VkSurfaceFormatKHR surfaceFormat = chooseSwapSurfaceFormat(swapChainSupport.formats);
	VkPresentModeKHR presentMode = chooseSwapPresentMode(swapChainSupport.presentModes);
	VkExtent2D extent = chooseSwapExtent(swapChainSupport.capabilities);

	uint32_t minImageCount = swapChainSupport.capabilities.maxImageCount > 0 ?
		std::min(swapChainSupport.capabilities.minImageCount + 1, swapChainSupport.capabilities.maxImageCount) :
		swapChainSupport.capabilities.minImageCount + 1;

	VkSwapchainCreateInfoKHR createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	createInfo.surface = renderWindow->surface;

	createInfo.minImageCount = minImageCount;
	createInfo.imageFormat = surfaceFormat.format;
	createInfo.imageColorSpace = surfaceFormat.colorSpace;
	createInfo.imageExtent = extent;
	createInfo.imageArrayLayers = 1;
	createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

	const uint32_t indices[] =
	{
		renderWindow->queueFamilyIndices.graphicsFamily.value(),
		renderWindow->queueFamilyIndices.presentFamily.value()
	};

	if (indices[ 0 ] != indices[ 1 ])
	{
		createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		createInfo.queueFamilyIndexCount = 2;
		createInfo.pQueueFamilyIndices = indices;
	}
	else
		createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;

	createInfo.preTransform = swapChainSupport.capabilities.currentTransform;
	createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR; // TODO: Revisit this for window blending
	createInfo.presentMode = presentMode;
	createInfo.clipped = VK_TRUE;
	createInfo.oldSwapchain = VK_NULL_HANDLE;

	if (vkCreateSwapchainKHR(device, &createInfo, nullptr, &renderWindow->swapChainKHR) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create swap chain");

	// Get the actual number of swap chain images we will use
	vkGetSwapchainImagesKHR(device, renderWindow->swapChainKHR, &renderWindow->numSwapChainImages, nullptr);

	// Allocate our elements dependant on the number of swap chain images
	renderWindow->swapChainImages = std::make_unique<VkImage[]>(renderWindow->numSwapChainImages);
	renderWindow->swapChainImageViews = std::make_unique<VkImageView[]>(renderWindow->numSwapChainImages);
	renderWindow->swapChainFramebuffers = std::make_unique<VkFramebuffer[]>(renderWindow->numSwapChainImages);

	// Initialize everything to VK_NULL_HANDLE
	for (uint32_t i = 0; i < renderWindow->numSwapChainImages; ++i)
	{
		renderWindow->swapChainImages[ i ] = VK_NULL_HANDLE;
		renderWindow->swapChainImageViews[ i ] = VK_NULL_HANDLE;
		renderWindow->swapChainFramebuffers[ i ] = VK_NULL_HANDLE;
	}

	vkGetSwapchainImagesKHR(device, renderWindow->swapChainKHR, &renderWindow->numSwapChainImages, &renderWindow->swapChainImages[ 0 ]);

	renderWindow->swapChainImageFormat = surfaceFormat.format;
	renderWindow->swapChainExtent = extent;
	renderWindow->aspectRatio = (float)extent.width / (float)extent.height;
}

void CGraphicsVK::CreateSwapChainImageViews(CRenderWindow *renderWindow)
{
	for (uint32_t i = 0; i < renderWindow->numSwapChainImages; ++i)
		renderWindow->swapChainImageViews[ i ] = CreateImageView2D(renderWindow->swapChainImages[ i ], renderWindow->swapChainImageFormat, VK_IMAGE_ASPECT_COLOR_BIT, 1);
}

void CGraphicsVK::FindDepthFormat(CRenderWindow *renderWindow)
{
	auto findSupportedFormat = [ renderWindow ](const std::vector< VkFormat > &candidates, VkImageTiling tiling, VkFormatFeatureFlags features)
	{
		for (const auto &format : candidates)
		{
			VkFormatProperties props = {};
			vkGetPhysicalDeviceFormatProperties(renderWindow->physicalDevice, format, &props);

			if ( tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features ) == features)
				return format;
			else if ( tiling == VK_IMAGE_TILING_OPTIMAL && ( props.optimalTilingFeatures & features ) == features )
				return format;
		}

		return VK_FORMAT_UNDEFINED;
	};

	renderWindow->depthFormat = findSupportedFormat(
		{ VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT },
		VK_IMAGE_TILING_OPTIMAL,
		VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT
	);

	if (renderWindow->depthFormat == VK_FORMAT_UNDEFINED) {
		throw std::runtime_error("[Vulkan]Failed to find supported depth format");
	}
}

void CGraphicsVK::CreateRenderPass(CRenderWindow *renderWindow)
{
	std::array<VkAttachmentDescription, 2> attachments = {};

	auto &colorAttachment = attachments[ 0 ];
	colorAttachment.format = renderWindow->swapChainImageFormat;
	colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
	colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

	auto &depthAttachment = attachments[ 1 ];
	depthAttachment.format = renderWindow->depthFormat;
	depthAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
	depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	depthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	depthAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

	VkAttachmentReference colorAttachmentRef = {};
	colorAttachmentRef.attachment = 0;
	colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	VkAttachmentReference depthAttachmentRef;
	depthAttachmentRef.attachment = 1;
	depthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

	VkSubpassDescription subpassDescription = {};
	subpassDescription.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subpassDescription.colorAttachmentCount = 1;
	subpassDescription.pColorAttachments = &colorAttachmentRef;
	subpassDescription.pDepthStencilAttachment = &depthAttachmentRef;

	VkSubpassDependency dependency = {};
	dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
	dependency.dstSubpass = 0;
	dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
	dependency.srcAccessMask = 0;
	dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	dependency.dependencyFlags = VK_DEPENDENCY_BY_REGION_BIT;

	VkRenderPassCreateInfo renderPassInfo = {};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	renderPassInfo.attachmentCount = static_cast< uint32_t >(attachments.size());
	renderPassInfo.pAttachments = attachments.data();
	renderPassInfo.subpassCount = 1;
	renderPassInfo.pSubpasses = &subpassDescription;
	renderPassInfo.dependencyCount = 1;
	renderPassInfo.pDependencies = &dependency;

	if (vkCreateRenderPass(device, &renderPassInfo, nullptr, &renderWindow->renderPass) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create render pass");
}

void CGraphicsVK::CreateCommandPool(CRenderWindow *renderWindow)
{
	VkCommandPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	poolInfo.queueFamilyIndex = renderWindow->queueFamilyIndices.graphicsFamily.value();
	poolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT; // Optional

	if (vkCreateCommandPool(device, &poolInfo, nullptr, &renderWindow->commandPool) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create command pool");
}

void CGraphicsVK::CreateDepthResources(CRenderWindow *renderWindow)
{
	renderWindow->depthImage = CreateImage2D(
		renderWindow,
		renderWindow->swapChainExtent.width, renderWindow->swapChainExtent.height,
		1,
		renderWindow->depthFormat,
		VK_IMAGE_TILING_OPTIMAL,
		VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
		VMA_MEMORY_USAGE_GPU_ONLY,
		renderWindow->depthImageAllocation);

	renderWindow->depthImageView = CreateImageView2D(renderWindow->depthImage, renderWindow->depthFormat, VK_IMAGE_ASPECT_DEPTH_BIT, 1);
	TransitionImageLayout(renderWindow, renderWindow->depthImage, renderWindow->depthFormat, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL, 1);
}

void CGraphicsVK::CreateFramebuffers(CRenderWindow *renderWindow)
{
	for (uint32_t i = 0; i < renderWindow->numSwapChainImages; ++i)
	{
		std::array<VkImageView, 2> attachments = { renderWindow->swapChainImageViews[ i ], renderWindow->depthImageView };

		VkFramebufferCreateInfo framebufferInfo = {};
		framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		framebufferInfo.renderPass = renderWindow->renderPass;
		framebufferInfo.attachmentCount = static_cast< uint32_t >( attachments.size() );
		framebufferInfo.pAttachments = attachments.data();
		framebufferInfo.width = renderWindow->swapChainExtent.width;
		framebufferInfo.height = renderWindow->swapChainExtent.height;
		framebufferInfo.layers = 1;

		if (vkCreateFramebuffer(device, &framebufferInfo, nullptr, &renderWindow->swapChainFramebuffers[ i ]) != VK_SUCCESS)
			throw std::runtime_error("[Vulkan]Failed to create framebuffers");
	}
}

void CGraphicsVK::CreateSyncObjects(CRenderWindow *renderWindow)
{
	VkSemaphoreCreateInfo semaphoreInfo = {};
	semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

	VkFenceCreateInfo fenceInfo = {};
	fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

	for ( std::size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i )
	{
		if (vkCreateSemaphore(device, &semaphoreInfo, nullptr, &renderWindow->imageAvailableSemaphores[ i ]) != VK_SUCCESS)
			throw std::runtime_error("[Vulkan]Failed to create sync objects");

		if (vkCreateSemaphore(device, &semaphoreInfo, nullptr, &renderWindow->renderFinishedSemaphores[ i ]) != VK_SUCCESS)
			throw std::runtime_error("[Vulkan]Failed to create sync objects");

		if (vkCreateFence(device, &fenceInfo, nullptr, &renderWindow->inFlightFences[ i ]) != VK_SUCCESS)
			throw std::runtime_error("[Vulkan]Failed to create sync objects");
	}
}

VkCommandBuffer CGraphicsVK::BeginSingleTimeCommands(CRenderWindow *renderWindow)
{
	VkCommandBufferAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandPool = renderWindow->commandPool;
	allocInfo.commandBufferCount = 1;

	VkCommandBuffer commandBuffer = VK_NULL_HANDLE;
	if (vkAllocateCommandBuffers(device, &allocInfo, &commandBuffer) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to allocate single use command buffer");

	VkCommandBufferBeginInfo beginInfo = {};
	beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

	vkBeginCommandBuffer(commandBuffer, &beginInfo);

	return commandBuffer;
}

void CGraphicsVK::EndSingleTimeCommands(CRenderWindow *renderWindow, VkCommandBuffer &commandBuffer)
{
	vkEndCommandBuffer(commandBuffer);

	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commandBuffer;

	vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE );
	vkQueueWaitIdle(graphicsQueue);

	vkFreeCommandBuffers(device, renderWindow->commandPool, 1, &commandBuffer);
}

VkImage CGraphicsVK::CreateImage2D(CRenderWindow *renderWindow, uint32_t width, uint32_t height, uint32_t mipLevels, VkFormat format, VkImageTiling tiling, VkImageUsageFlags imageUsage, VmaMemoryUsage memoryUsage, VmaAllocation &allocation)
{
	VkImageCreateInfo imageInfo = {};
	imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
	imageInfo.imageType = VK_IMAGE_TYPE_2D;
	imageInfo.extent.width = width;
	imageInfo.extent.height = height;
	imageInfo.extent.depth = 1;
	imageInfo.mipLevels = mipLevels;
	imageInfo.arrayLayers = 1;
	imageInfo.format = format;
	imageInfo.tiling = tiling;
	imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	imageInfo.usage = imageUsage;
	imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
	imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	VmaAllocationCreateInfo allocInfo = {};
	allocInfo.usage = memoryUsage;

	VkImage image = VK_NULL_HANDLE;

	if (vmaCreateImage(renderWindow->allocator, &imageInfo, &allocInfo, &image, &allocation, nullptr) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create image");

	return image;
}

VkImageView CGraphicsVK::CreateImageView2D(VkImage image, VkFormat format, VkImageAspectFlags aspectFlags, uint32_t mipLevels)
{
	VkImageViewCreateInfo viewInfo = {};
	viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	viewInfo.image = image;
	viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
	viewInfo.format = format;
	viewInfo.subresourceRange.aspectMask = aspectFlags;
	viewInfo.subresourceRange.baseMipLevel = 0;
	viewInfo.subresourceRange.levelCount = mipLevels;
	viewInfo.subresourceRange.baseArrayLayer = 0;
	viewInfo.subresourceRange.layerCount = 1;

	VkImageView imageView = VK_NULL_HANDLE;
	if ( vkCreateImageView(device, &viewInfo, nullptr, &imageView) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create image view");

	return imageView;
}

void CGraphicsVK::CopyBufferToImage(CRenderWindow *renderWindow, VkBuffer buffer, VkImage image, uint32_t width, uint32_t height)
{
	std::vector<VkBufferImageCopy> bufferCopyRegions;

	VkBufferImageCopy region = {};
	region.bufferOffset = 0;
	region.bufferRowLength = 0;
	region.bufferImageHeight = 0;
	region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	region.imageSubresource.mipLevel = 0;
	region.imageSubresource.baseArrayLayer = 0;
	region.imageSubresource.layerCount = 1;
	region.imageOffset = { 0, 0, 0 };
	region.imageExtent = { width, height, 1 };
	bufferCopyRegions.push_back( region );

	VkCommandBuffer commandBuffer = BeginSingleTimeCommands(renderWindow);
		vkCmdCopyBufferToImage(commandBuffer, buffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, static_cast<uint32_t>(bufferCopyRegions.size()), bufferCopyRegions.data());
	EndSingleTimeCommands(renderWindow, commandBuffer);
}

void CGraphicsVK::TransitionImageLayout(CRenderWindow *renderWindow, VkImage image, VkFormat format, VkImageLayout oldLayout, VkImageLayout newLayout, uint32_t mipLevels)
{
	VkCommandBuffer commandBuffer = BeginSingleTimeCommands(renderWindow);
	VkImageMemoryBarrier barrier = {};
	barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	barrier.oldLayout = oldLayout;
	barrier.newLayout = newLayout;
	barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.image = image;
			
	if (newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
	{
		barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
		auto hasStencilComponent = [](VkFormat format) { return ( format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT ); };
				
		if ( hasStencilComponent( format ) )
			barrier.subresourceRange.aspectMask |= VK_IMAGE_ASPECT_STENCIL_BIT;
	}
	else {
		barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	}

	barrier.subresourceRange.baseMipLevel = 0;
	barrier.subresourceRange.levelCount = mipLevels;
	barrier.subresourceRange.baseArrayLayer = 0;
	barrier.subresourceRange.layerCount = 1;
	barrier.srcAccessMask = 0; // TODO
	barrier.dstAccessMask = 0; // TODO

	VkPipelineStageFlags sourceStage, destinationStage;

	if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
	{
		barrier.srcAccessMask = 0;
		barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;

		sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
	}
	else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
	{
		barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
		barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

		sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
		destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
	}
	else if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
	{
		barrier.srcAccessMask = 0;
		barrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

		sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
		destinationStage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
	}
	else
		throw std::runtime_error("[Vulkan]Unsupported layout transition");

	vkCmdPipelineBarrier(
		commandBuffer,
		sourceStage, destinationStage,
		0,
		0, nullptr, 
		0, nullptr, 
		1, &barrier);

	EndSingleTimeCommands(renderWindow, commandBuffer);
}

void CGraphicsVK::CreateBuffer(CRenderWindow *renderWindow, VkDeviceSize size, VkBufferUsageFlags bufferUsage, VmaMemoryUsage memoryUsage, VkBuffer &buffer, VmaAllocation &allocation)
{
	VkBufferCreateInfo bufferInfo = {};
	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferInfo.size = size;
	bufferInfo.usage = bufferUsage;
	bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	VmaAllocationCreateInfo allocInfo = {};
	allocInfo.usage = memoryUsage;

	if (vmaCreateBuffer(renderWindow->allocator, &bufferInfo, &allocInfo, &buffer, &allocation, nullptr) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create buffer");
}

void CGraphicsVK::DeleteBuffer(CRenderWindow *renderWindow, VkBuffer buffer, VmaAllocation allocation)
{
	vmaDestroyBuffer(renderWindow->allocator, buffer, allocation);
}

void CGraphicsVK::CopyBuffer(CRenderWindow *renderWindow, const VkBuffer &srcBuffer, VkBuffer &dstBuffer, VkDeviceSize size)
{
	VkCommandBuffer commandBuffer = BeginSingleTimeCommands(renderWindow);
		VkBufferCopy copyRegion = {};
		copyRegion.size = size;
		vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &copyRegion);
	EndSingleTimeCommands(renderWindow, commandBuffer);
}