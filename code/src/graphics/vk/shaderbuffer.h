#pragma once

#include <vulkan/vulkan.h>
#include <vk_mem_alloc.h>
#include <cstring>

#include "renderwindow.h"

class CShaderBuffer
{
public:

	template <typename T>
	void UpdateBuffer(uint32_t imageIndex, const T &src, std::size_t size)
	{
		VmaAllocation &allocation = bufferAllocation[ imageIndex ];

		if (allocation != VK_NULL_HANDLE)
		{
			void *data = nullptr;
			vmaMapMemory(renderWindow->allocator, allocation, &data);
				std::memcpy(data, &src, size);
			vmaUnmapMemory(renderWindow->allocator, allocation);
		}
	}

	CRenderWindow *renderWindow = nullptr;

	std::vector<VkBuffer> buffer;
	std::vector<VmaAllocation> bufferAllocation;
	VkDeviceSize bufferSize = {};

	VkDescriptorType descriptorType = {};
};