#pragma once

#include "shadervk.h"
#include <vector>
#include <unordered_map>
#include <string>
#include <memory>
#include <filesystem>

#include "shaderbuffer.h"
#include "graphicsvk_definitions.h"

class CRendererVK;

class CShaderSystemVK
{
public:
	CShaderVK *GetShader(const std::string &name) { return shaders[ name ].get(); }

	void OnWindowResize();

	void LoadShaders();
	void UnloadShaders();

	VkShaderModule CreateShaderModule(const std::filesystem::path shaderPath);

	template <typename T>
	void CreateShader(const std::string &name)
	{
		auto pair = shaders.emplace(std::make_pair(name, std::make_unique<T>()));

		CShaderVK *shader = pair.first->second.get();
		shader->shaderName = pair.first->first;
		shader->shaderSystem = this;

		shader->CreateDescriptorSetLayout();
		shader->CreateGraphicsPipelineLayout();
		shader->CreateGraphicsPipeline();

		shader->InitShader();
	}

	template <typename T>
	CShaderBuffer *CreateUniformBuffer()
	{
		return CreateShaderBuffer_Internal(sizeof(T), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER);
	}

	template <typename T>
	CShaderBuffer *CreateStorageBuffer()
	{
		return CreateShaderBuffer_Internal(sizeof(T), VK_BUFFER_USAGE_STORAGE_BUFFER_BIT, VK_DESCRIPTOR_TYPE_STORAGE_BUFFER);
	}

	void DeleteShaderBuffer(CShaderBuffer *shaderBuffer);

	CRendererVK *renderer = nullptr;
	std::unordered_map<std::string, std::unique_ptr<CShaderVK>> shaders;
	std::vector<std::unique_ptr<CShaderBuffer>> shaderBuffers;

private:
	CShaderBuffer *CreateShaderBuffer_Internal(std::size_t bufferSize, VkBufferUsageFlags bufferUsage, VkDescriptorType descriptorType);
};