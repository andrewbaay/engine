#pragma once

#include <vulkan/vulkan.h>
#include <vk_mem_alloc.h>

class CTextureVK
{
public:
	VkImageView GetImageView() const { return textureImageView; }
	VkSampler GetSampler() const { return textureSampler; }

	VkImage textureImage = VK_NULL_HANDLE;
	VmaAllocation textureImageAllocation = VK_NULL_HANDLE;
	VkImageView textureImageView = VK_NULL_HANDLE;
	VkSampler textureSampler = VK_NULL_HANDLE;
	uint32_t mipLevels = 1;
};