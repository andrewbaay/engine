#pragma once

#include <optional>
#include <vulkan/vulkan.h>

class CRenderWindow;

struct QueueFamilyIndices
{
	std::optional<uint32_t> graphicsFamily;
	std::optional<uint32_t> presentFamily;

	bool IsComplete() const { return ( graphicsFamily.has_value() && presentFamily.has_value() ); }
};

QueueFamilyIndices FindQueueFamilies(CRenderWindow *renderWindow, VkPhysicalDevice device);