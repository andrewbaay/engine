#include "scenevk.h"
#include "log.h"
#include "renderervk_impl.h"
#include "graphicsvk.h"
#include "shadersystem.h"
#include "materialinstance.h"
#include "shadervk.h"

#include <cmath>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#define STB_IMAGE_IMPLEMENTATION
#include "stb/stb_image.h"

IView *CSceneVK::CreateView(Camera *camera)
{
	views.emplace_back(std::make_unique<CViewVK>());

	CViewVK *view = views.back().get();
	view->camera = camera;
	view->scene = this;

	return view;
}

void CSceneVK::DeleteView(IView *baseView)
{
	auto it = std::find_if(views.begin(), views.end(), [&](const std::unique_ptr<CViewVK> &viewHandle) { return baseView == viewHandle.get(); });

	if (it != views.end())
		views.erase(it);
}

void CSceneVK::LoadDefaultResources()
{
	textureError = LoadTexture("textures/error.png", ETextureFallback::Error);

	if (!textureError)
		throw std::runtime_error("Failed to load default error texture!");

	materials.emplace_back(std::make_unique<CMaterialVK>());

	materialError = materials.back().get();
	materialError->SetShader(renderer->shaderSystem->GetShader("static_mesh"));
	materialError->SetTexture("diffuse", "textures/error.png");

	cube = LoadModel("models/cube.obj");

	if (!cube)
		throw std::runtime_error("Failed to load cube!");
}

// TODO: Move all of this garbage somewhere that makes actual sense!

void CSceneVK::UnloadTextures()
{
	// NOTE: We except vkDeviceWaitIdle to be called before we get here
	CRenderWindow *renderWindow = renderer->renderWindow;
	CGraphicsVK *graphics = renderer->graphics;

	for (auto &[id, textureHandle] : textures)
	{
		CTextureVK *texture = textureHandle.get();
		if (texture->textureSampler != VK_NULL_HANDLE)
		{
			vkDestroySampler(graphics->device, texture->textureSampler, nullptr);
			texture->textureSampler = VK_NULL_HANDLE;
		}

		if (texture->textureImageView != VK_NULL_HANDLE)
		{
			vkDestroyImageView(graphics->device, texture->textureImageView, nullptr);
			texture->textureImageView = VK_NULL_HANDLE;
		}

		if (texture->textureImage != VK_NULL_HANDLE)
		{
			vmaDestroyImage(renderWindow->allocator, texture->textureImage, texture->textureImageAllocation);
			texture->textureImage = VK_NULL_HANDLE;
			texture->textureImageAllocation = VK_NULL_HANDLE;
		}
	}

	textures.clear();
}

void CSceneVK::UnloadModels()
{
	// NOTE: We except vkDeviceWaitIdle to be called before we get here

	CGraphicsVK *graphics = renderer->graphics;
	CRenderWindow *renderWindow = renderer->renderWindow;
	
	for (auto &[id, modelHandle] : models)
	{
		for (auto &mesh : modelHandle->meshes)
		{
			CMaterialInstanceVK *materialInstance = mesh->materialInstance.get();
			for (auto &[binding, shaderBuffer] : materialInstance->shaderBuffers)
			{
				renderer->shaderSystem->DeleteShaderBuffer(shaderBuffer);
				shaderBuffer = nullptr;
			}

			materialInstance->shaderBuffers.clear();
			
			if (materialInstance->descriptorPool != VK_NULL_HANDLE)
			{
				vkDestroyDescriptorPool(graphics->device, materialInstance->descriptorPool, nullptr);
				materialInstance->descriptorPool = VK_NULL_HANDLE;
			}

			if (mesh->indexBuffer != VK_NULL_HANDLE)
			{
				vmaDestroyBuffer(renderWindow->allocator, mesh->indexBuffer, mesh->indexBufferAllocation);
				mesh->indexBuffer = VK_NULL_HANDLE;
				mesh->indexBufferAllocation = VK_NULL_HANDLE;
			}

			if (mesh->vertexBuffer != VK_NULL_HANDLE)
			{
				vmaDestroyBuffer(renderWindow->allocator, mesh->vertexBuffer, mesh->vertexBufferAllocation);
				mesh->vertexBuffer = VK_NULL_HANDLE;
				mesh->vertexBufferAllocation = VK_NULL_HANDLE;
			}
		}
	}

	models.clear();
}

CTextureVK *CSceneVK::LoadTexture(const std::filesystem::path &path, ETextureFallback textureFallback)
{
	if (path.empty())
		return GetFallbackTexture(textureFallback);

	// TODO: Better caching
	if (auto it = textures.find(path.generic_string()); it != textures.end())
		return it->second.get();

	// TODO: Mipmaps
	const bool generateMipMaps = false;
	const VkDeviceSize numChannels = 4; // Always RGBA

	int width = 0;
	int height = 0;
	int numComponents = 0;

	stbi_uc *pixels = stbi_load(path.string().c_str(), &width, &height, &numComponents, STBI_rgb_alpha);

	if (!pixels)
	{
		Log::Print("Failed to load texture {}\n", path.string());
		return GetFallbackTexture(textureFallback);
	}

	std::unique_ptr<CTextureVK> textureHandle = std::make_unique<CTextureVK>();
	CTextureVK *texture = textureHandle.get();

	if (generateMipMaps)
		texture->mipLevels = static_cast<uint32_t>(std::floor(std::log2(std::max(width, height)))) + 1;

	CGraphicsVK *graphics = renderer->graphics;
	CRenderWindow *renderWindow = renderer->renderWindow;

	VkDeviceSize imageSize = (VkDeviceSize)width * (VkDeviceSize)height * numChannels;

	VkBuffer stagingBuffer = VK_NULL_HANDLE;
	VmaAllocation stagingBufferAllocation = VK_NULL_HANDLE;

	VkBufferCreateInfo bufferInfo = {};
	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferInfo.size = imageSize;
	bufferInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
	bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	VmaAllocationCreateInfo allocInfo = {};
	allocInfo.usage = VMA_MEMORY_USAGE_CPU_ONLY;

	if (vmaCreateBuffer(renderWindow->allocator, &bufferInfo, &allocInfo, &stagingBuffer, &stagingBufferAllocation, nullptr) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create buffer");

	void *data = nullptr;
	vmaMapMemory(renderWindow->allocator, stagingBufferAllocation, &data);
		std::memcpy(data, pixels, static_cast<size_t>(imageSize));
	vmaUnmapMemory(renderWindow->allocator, stagingBufferAllocation);

	texture->textureImage = graphics->CreateImage2D(renderWindow,
		width, height,
		texture->mipLevels,
		VK_FORMAT_R8G8B8A8_UNORM,
		VK_IMAGE_TILING_OPTIMAL,
		VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
		VMA_MEMORY_USAGE_GPU_ONLY,
		texture->textureImageAllocation);

	graphics->TransitionImageLayout(renderWindow, texture->textureImage, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, texture->mipLevels);
	graphics->CopyBufferToImage(renderWindow, stagingBuffer, texture->textureImage, width, height);

	if (generateMipMaps)
	{
		// TODO:
	}
	else
	{
		graphics->TransitionImageLayout(renderWindow, texture->textureImage, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL, texture->mipLevels);
	}

	vmaDestroyBuffer(renderWindow->allocator, stagingBuffer, stagingBufferAllocation);

	// Create Texture Image View
	texture->textureImageView = graphics->CreateImageView2D(texture->textureImage, VK_FORMAT_R8G8B8A8_UNORM, VK_IMAGE_ASPECT_COLOR_BIT, texture->mipLevels);

	// Create Texture Sampler
	VkSamplerCreateInfo samplerInfo = {};
	samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	samplerInfo.magFilter = VK_FILTER_LINEAR;
	samplerInfo.minFilter = VK_FILTER_LINEAR;
	samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerInfo.anisotropyEnable = VK_TRUE;
	samplerInfo.maxAnisotropy = 16.0f;
	samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
	samplerInfo.unnormalizedCoordinates = VK_FALSE;
	samplerInfo.compareEnable = VK_FALSE;
	samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
	samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
	samplerInfo.mipLodBias = 0.0f;
	samplerInfo.minLod = 0.0f;
	samplerInfo.maxLod = static_cast<float>(texture->mipLevels);

	if (vkCreateSampler(graphics->device, &samplerInfo, nullptr, &texture->textureSampler) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create sampler");

	stbi_image_free(pixels);
	textures[ path.generic_string() ] = std::move(textureHandle);

	return texture;
}

CModelVK *CSceneVK::LoadModel(const std::filesystem::path &path)
{
	// TODO: Better caching
	if (auto it = models.find(path.generic_string()); it != models.end())
		return it->second.get();

	Assimp::Importer Importer;
	const aiScene *aScene = Importer.ReadFile(path.string(), aiProcess_Triangulate | aiProcess_MakeLeftHanded | aiProcess_GenNormals | aiProcess_CalcTangentSpace | aiProcess_LimitBoneWeights);

	if (!aScene)
		throw std::runtime_error("Failed to load model\n");

	Log::Print("Loading model file {}\n", path.string());

	CGraphicsVK *graphics = renderer->graphics;
	CRenderWindow *renderWindow = renderer->renderWindow;

	std::unique_ptr<CModelVK> modelHandle = std::make_unique<CModelVK>();
	CShaderVK *shader = renderer->shaderSystem->GetShader("static_mesh");

	for (unsigned int meshIndex = 0; meshIndex < aScene->mNumMeshes; ++meshIndex)
	{
		CMaterialVK *material = materialError;
		const aiMesh *aMesh = aScene->mMeshes[ meshIndex ];

		aiMaterial *aMaterial = aScene->mMaterials[ aMesh->mMaterialIndex ];
		
		if (aMaterial != nullptr)
		{
			materials.emplace_back(std::make_unique<CMaterialVK>());
			material = materials.back().get();

			material->SetShader(shader);

			if (aMaterial->GetTextureCount(aiTextureType_DIFFUSE) > 0)
			{
				aiString aDiffusePath;
				aMaterial->GetTexture(aiTextureType::aiTextureType_DIFFUSE, 0, &aDiffusePath);
				std::filesystem::path diffusePath = path.parent_path() /= aDiffusePath.C_Str();

				material->SetTexture("diffuse", diffusePath);
			}
		}

		std::vector<Vertex> vertices(aMesh->mNumVertices);
		std::vector<uint32_t> indices(aMesh->mNumFaces * 3);

		for (unsigned int v = 0; v < aMesh->mNumVertices; ++v)
		{
			constexpr glm::vec4 defaultColor = { 1.0f, 1.0f, 1.0f, 1.0f };

			if (aMesh->HasPositions())
				vertices[v].position = { aMesh->mVertices[v].x, aMesh->mVertices[v].y, aMesh->mVertices[v].z };

			if (aMesh->HasNormals())
				vertices[v].normal = { aMesh->mNormals[v].x, aMesh->mNormals[v].y, aMesh->mNormals[v].z };

			if (aMesh->HasTangentsAndBitangents())
			{
				vertices[v].tangent = { aMesh->mTangents[v].x, aMesh->mTangents[v].y, aMesh->mTangents[v].z };
				vertices[v].bitangent = { aMesh->mBitangents[v].x, aMesh->mBitangents[v].y, aMesh->mBitangents[v].z };
			}

			if (aMesh->HasTextureCoords(0))
				vertices[v].uv = { aMesh->mTextureCoords[0][v].x, aMesh->mTextureCoords[0][v].y };

			if (aMesh->HasVertexColors(0))
				vertices[v].color = { aMesh->mColors[0][v].r, aMesh->mColors[0][v].g, aMesh->mColors[0][v].b, aMesh->mColors[0][v].a };
			else
				vertices[v].color = defaultColor;
		}

		if (aMesh->HasBones())
		{
			std::unordered_map<unsigned int, unsigned int> vertexBoneWeights;

			for (unsigned int bone = 0; bone < aMesh->mNumBones; ++bone)
			{
				for (unsigned int weight = 0; weight < aMesh->mBones[bone]->mNumWeights; ++weight)
				{
					Vertex &vertex = vertices[aMesh->mBones[bone]->mWeights[weight].mVertexId];
					auto &numWeights = vertexBoneWeights[aMesh->mBones[bone]->mWeights[weight].mVertexId];
					if (numWeights < 4)
					{
						vertex.boneindex[numWeights] = bone;
						vertex.boneweight[numWeights] = aMesh->mBones[bone]->mWeights[weight].mWeight;

						++numWeights;
					}
				}
			}
		}

		for (unsigned int face = 0; face < aMesh->mNumFaces; ++face)
		{
			for (unsigned int idx = 0; idx < aMesh->mFaces[face].mNumIndices; ++idx)
			{
				const unsigned int absIdx = (face * 3) + idx;
				indices[absIdx] = aMesh->mFaces[face].mIndices[idx];
			}
		}

		modelHandle->meshes.emplace_back(std::make_unique<CMeshVK>());

		CMeshVK *mesh = modelHandle->meshes.back().get();
		mesh->renderer = renderer;

		auto CreateVertexBuffer = [&]()
		{
			if (vertices.size() == 0)
				return;

			const VkDeviceSize bufferSize = static_cast<VkDeviceSize>(vertices.size()) * sizeof(Vertex);
			mesh->vertexCount = static_cast<uint32_t>(vertices.size());

			VkBuffer stagingBuffer = VK_NULL_HANDLE;
			VmaAllocation stagingBufferAllocation = VK_NULL_HANDLE;

			graphics->CreateBuffer(renderWindow, bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VMA_MEMORY_USAGE_CPU_ONLY, stagingBuffer, stagingBufferAllocation);

			void *data = nullptr;
			vmaMapMemory(renderWindow->allocator, stagingBufferAllocation, &data);
				std::memcpy(data, vertices.data(), static_cast<size_t>(bufferSize));
			vmaUnmapMemory(renderWindow->allocator, stagingBufferAllocation);

			graphics->CreateBuffer(renderWindow, bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VMA_MEMORY_USAGE_GPU_ONLY, mesh->vertexBuffer, mesh->vertexBufferAllocation);
			graphics->CopyBuffer(renderWindow, stagingBuffer, mesh->vertexBuffer, bufferSize);

			graphics->DeleteBuffer(renderWindow, stagingBuffer, stagingBufferAllocation);
		};

		auto CreateIndexBuffer = [&]()
		{
			if ( indices.size() == 0 )
				return;

			VkDeviceSize bufferSize = sizeof(indices[ 0 ]) * indices.size();
			mesh->indexCount = static_cast<uint32_t>(indices.size());

			VkBuffer stagingBuffer = VK_NULL_HANDLE;
			VmaAllocation stagingBufferAllocation = VK_NULL_HANDLE;

			graphics->CreateBuffer(renderWindow, bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VMA_MEMORY_USAGE_CPU_ONLY, stagingBuffer, stagingBufferAllocation);

			void *data = nullptr;

			vmaMapMemory(renderWindow->allocator, stagingBufferAllocation, &data);
				std::memcpy(data, indices.data(), static_cast<size_t>(bufferSize));
			vmaUnmapMemory(renderWindow->allocator, stagingBufferAllocation);

			graphics->CreateBuffer(renderWindow, bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VMA_MEMORY_USAGE_GPU_ONLY, mesh->indexBuffer, mesh->indexBufferAllocation);
			graphics->CopyBuffer(renderWindow, stagingBuffer, mesh->indexBuffer, bufferSize);

			graphics->DeleteBuffer(renderWindow, stagingBuffer, stagingBufferAllocation);
		};

		CreateVertexBuffer();
		CreateIndexBuffer();

		mesh->materialInstance = std::make_unique<CMaterialInstanceVK>();
		mesh->materialInstance->material = material;

		shader->InitMaterialInstance(this, mesh->materialInstance.get());

		auto SetupMaterialInstance = [&]()
		{
			CMaterialInstanceVK *materialInstance = mesh->materialInstance.get();
			std::vector<VkDescriptorPoolSize> poolSizes;

			for (std::size_t i = 0; i < materialInstance->samplers.size(); ++i)
			{
				VkDescriptorPoolSize poolSize = {};
				poolSize.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
				poolSize.descriptorCount = MAX_FRAMES_IN_FLIGHT;

				poolSizes.emplace_back(poolSize);
			}

			for (auto &[binding, shaderBuffer] : materialInstance->shaderBuffers)
			{
				VkDescriptorPoolSize poolSize = {};

				poolSize.type = shaderBuffer->descriptorType;
				poolSize.descriptorCount = MAX_FRAMES_IN_FLIGHT;

				poolSizes.emplace_back(poolSize);
			}

			VkDescriptorPoolCreateInfo poolInfo = {};
			poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
			poolInfo.poolSizeCount = static_cast<uint32_t>(poolSizes.size());
			poolInfo.pPoolSizes = poolSizes.data();
			poolInfo.maxSets = MAX_FRAMES_IN_FLIGHT;

			if (vkCreateDescriptorPool(graphics->device, &poolInfo, nullptr, &materialInstance->descriptorPool) != VK_SUCCESS)
				throw std::runtime_error("[Vulkan]Failed to create descriptor pool");

			const std::vector<VkDescriptorSetLayout> layouts(MAX_FRAMES_IN_FLIGHT, shader->descriptorSetLayout);

			VkDescriptorSetAllocateInfo allocInfo = {};
			allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
			allocInfo.descriptorPool = materialInstance->descriptorPool;
			allocInfo.descriptorSetCount = MAX_FRAMES_IN_FLIGHT;
			allocInfo.pSetLayouts = layouts.data();

			materialInstance->descriptorSets.resize(MAX_FRAMES_IN_FLIGHT);
			if (vkAllocateDescriptorSets(graphics->device, &allocInfo, materialInstance->descriptorSets.data()) != VK_SUCCESS)
				throw std::runtime_error("[Vulkan]Failed to allocate descriptor sets");

			for (std::size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i)
			{
				for (auto &[binding, shaderBuffer] : materialInstance->shaderBuffers)
				{
					VkDescriptorBufferInfo bufferInfo = {};
					bufferInfo.buffer = shaderBuffer->buffer[i];
					bufferInfo.offset = 0;
					bufferInfo.range = shaderBuffer->bufferSize;

					VkWriteDescriptorSet descriptorWrite = {};
					descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
					descriptorWrite.dstSet = materialInstance->descriptorSets[i];
					descriptorWrite.dstBinding = binding;
					descriptorWrite.dstArrayElement = 0;
					descriptorWrite.descriptorType = shaderBuffer->descriptorType;
					descriptorWrite.descriptorCount = 1;
					descriptorWrite.pBufferInfo = &bufferInfo;

					vkUpdateDescriptorSets(graphics->device, 1, &descriptorWrite, 0, nullptr);
				}

				for (auto &[binding, texture] : materialInstance->samplers)
				{
					VkDescriptorImageInfo imageInfo = {};
					imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
					imageInfo.imageView = texture->GetImageView();
					imageInfo.sampler = texture->GetSampler();

					VkWriteDescriptorSet descriptorWrite = {};
					descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
					descriptorWrite.dstSet = materialInstance->descriptorSets[i];
					descriptorWrite.dstBinding = binding;
					descriptorWrite.dstArrayElement = 0;
					descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
					descriptorWrite.descriptorCount = 1;
					descriptorWrite.pImageInfo = &imageInfo;

					vkUpdateDescriptorSets(graphics->device, 1, &descriptorWrite, 0, nullptr);
				}
			}
		};
		
		SetupMaterialInstance();
	}

	CModelVK *model = modelHandle.get();
	models[path.generic_string()] = std::move(modelHandle);

	return model;
}