#pragma once

#include "vertexvk.h"
#include "materialinstance.h"
#include <vulkan/vulkan.h>
#include <vk_mem_alloc.h>
#include <memory>

class CRendererVK;

class CMeshVK
{
public:
	CRendererVK *renderer = nullptr;
	std::unique_ptr<CMaterialInstanceVK> materialInstance;

	VkBuffer vertexBuffer = VK_NULL_HANDLE;
	VkBuffer indexBuffer = VK_NULL_HANDLE;
	VmaAllocation vertexBufferAllocation = VK_NULL_HANDLE;
	VmaAllocation indexBufferAllocation = VK_NULL_HANDLE;

	uint32_t vertexCount = 0;
	uint32_t indexCount = 0;
};