#include "shadersystem.h"
#include "shader_staticmesh.h"
#include "renderervk_impl.h"
#include "graphicsvk.h"

#include <fstream>

void CShaderSystemVK::OnWindowResize()
{
	// NOTE: We assume vkDeviceWaitIdle has already been called
	CGraphicsVK *graphics = renderer->graphics;

	for (auto &[id, shaderHandle] : shaders)
	{
		if (shaderHandle->pipeline != VK_NULL_HANDLE)
		{
			vkDestroyPipeline(graphics->device, shaderHandle->pipeline, nullptr);
			shaderHandle->pipeline = VK_NULL_HANDLE;

			shaderHandle->CreateGraphicsPipeline();
		}
	}
}

void CShaderSystemVK::LoadShaders()
{
	CreateShader<CShaderStaticMesh>("static_mesh");
}

void CShaderSystemVK::UnloadShaders()
{
	// NOTE: We assume vkDeviceWaitIdle has already been called
	CGraphicsVK *graphics = renderer->graphics;

	for (auto &[id, shaderHandle] : shaders)
	{
		if (shaderHandle->pipeline != VK_NULL_HANDLE)
		{
			vkDestroyPipeline(graphics->device, shaderHandle->pipeline, nullptr);
			shaderHandle->pipeline = VK_NULL_HANDLE;
		}

		if (shaderHandle->pipelineLayout != VK_NULL_HANDLE)
		{
			vkDestroyPipelineLayout(graphics->device, shaderHandle->pipelineLayout, nullptr);
			shaderHandle->pipelineLayout = VK_NULL_HANDLE;
		}

		if (shaderHandle->descriptorSetLayout != VK_NULL_HANDLE)
		{
			vkDestroyDescriptorSetLayout(graphics->device, shaderHandle->descriptorSetLayout, nullptr);
			shaderHandle->descriptorSetLayout = VK_NULL_HANDLE;
		}
	}

	shaders.clear();
}

VkShaderModule CShaderSystemVK::CreateShaderModule(const std::filesystem::path shaderPath)
{
	// TODO: Error handling
	auto fileSize = std::filesystem::file_size(shaderPath);

	std::vector<char> code(fileSize);
	std::ifstream(shaderPath, std::ios::binary).read(code.data(), code.size());

	CGraphicsVK *graphics = renderer->graphics;

	VkShaderModuleCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	createInfo.codeSize = code.size();
	createInfo.pCode = reinterpret_cast<const uint32_t*>(code.data());

	VkShaderModule shaderModule = VK_NULL_HANDLE;
	if (vkCreateShaderModule(graphics->device, &createInfo, nullptr, &shaderModule) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create shader module");

	return shaderModule;
}

void CShaderSystemVK::DeleteShaderBuffer(CShaderBuffer *shaderBuffer)
{
	auto it = std::find_if(shaderBuffers.begin(), shaderBuffers.end(), [&](const std::unique_ptr<CShaderBuffer> &shaderBufferHandle) { return shaderBuffer == shaderBufferHandle.get(); } );
	if (it != shaderBuffers.end())
	{
		CGraphicsVK *graphics = renderer->graphics;
		CRenderWindow *renderWindow = renderer->renderWindow;

		for (std::size_t i = 0; i < shaderBuffer->buffer.size(); ++i)
			graphics->DeleteBuffer(renderWindow, shaderBuffer->buffer[i], shaderBuffer->bufferAllocation[i]);

		shaderBuffers.erase(it);
	}
}

CShaderBuffer *CShaderSystemVK::CreateShaderBuffer_Internal(std::size_t bufferSize, VkBufferUsageFlags bufferUsage, VkDescriptorType descriptorType)
{
	CGraphicsVK *graphics = renderer->graphics;
	CRenderWindow *renderWindow = renderer->renderWindow;

	shaderBuffers.emplace_back(std::make_unique<CShaderBuffer>());
	CShaderBuffer *shaderBuffer = shaderBuffers.back().get();
	shaderBuffer->renderWindow = renderWindow;

	shaderBuffer->buffer.resize(MAX_FRAMES_IN_FLIGHT);
	shaderBuffer->bufferAllocation.resize(MAX_FRAMES_IN_FLIGHT);
	shaderBuffer->bufferSize = static_cast<VkDeviceSize>(bufferSize);
	shaderBuffer->descriptorType = descriptorType;

	for (std::size_t i = 0; i < shaderBuffer->buffer.size(); ++i)
		graphics->CreateBuffer(renderWindow, shaderBuffer->bufferSize, bufferUsage, VMA_MEMORY_USAGE_CPU_TO_GPU, shaderBuffer->buffer[i], shaderBuffer->bufferAllocation[i]);

	return shaderBuffer;
}