#pragma once

#include "glm/glm.hpp"

#include <vulkan/vulkan.h>

#include <cstdint>
#include <cstddef>
#include <vector>

struct Vertex
{
	alignas(16) glm::vec3 position;
	alignas(16) glm::vec3 normal;
	alignas(16) glm::vec3 tangent;
	alignas(16) glm::vec3 bitangent;
	alignas(16) glm::vec2 uv;
	alignas(16) glm::vec4 color;
	alignas(16) glm::uvec4 boneindex;
	alignas(16) glm::vec4 boneweight; // 8th attribute

	static VkVertexInputBindingDescription ToInputBindingDescription(uint32_t binding)
	{
		VkVertexInputBindingDescription description = {};
		description.binding = binding;
		description.stride = sizeof(Vertex);
		description.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

		return description;
	}

	static std::vector<VkVertexInputAttributeDescription> ToInputAttributeDescriptions(uint32_t binding)
	{
		std::vector<VkVertexInputAttributeDescription> descriptions(8);

		for (std::size_t i = 0; i < descriptions.size(); ++i)
		{
			auto &desc = descriptions[i];
			desc.binding = binding;
			desc.location = static_cast<uint32_t>(i);
		}

		descriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;		// Position
		descriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;		// Normal
		descriptions[2].format = VK_FORMAT_R32G32B32_SFLOAT;		// Tangent
		descriptions[3].format = VK_FORMAT_R32G32B32_SFLOAT;		// Bitangent
		descriptions[4].format = VK_FORMAT_R32G32_SFLOAT;			// UV
		descriptions[5].format = VK_FORMAT_R32G32B32A32_SFLOAT;		// Color
		descriptions[6].format = VK_FORMAT_R32G32B32A32_UINT;		// Bone indices
		descriptions[7].format = VK_FORMAT_R32G32B32A32_SFLOAT;		// Bone weights

		descriptions[0].offset = offsetof(Vertex, position);
		descriptions[1].offset = offsetof(Vertex, normal);
		descriptions[2].offset = offsetof(Vertex, tangent);
		descriptions[3].offset = offsetof(Vertex, bitangent);
		descriptions[4].offset = offsetof(Vertex, uv);
		descriptions[5].offset = offsetof(Vertex, color);
		descriptions[6].offset = offsetof(Vertex, boneindex);
		descriptions[7].offset = offsetof(Vertex, boneweight);

		return descriptions;
	}
};