#pragma once

#include "modelvk.h"
#include "texturevk.h"
#include "materialvk.h"
#include "shaderbuffer.h"
#include "viewvk.h"

#include <memory>
#include <vector>
#include <unordered_map>
#include <filesystem>

// TODO: MOVE THIS!
enum class ETextureFallback
{
	Error
};

class CRendererVK;

class CSceneVK : public CBaseScene
{
public:
	IView *CreateView(Camera *camera) override;
	void DeleteView(IView *viewHandle) override;

	void LoadDefaultResources();

	void UnloadTextures();
	void UnloadModels();

	CTextureVK *LoadTexture(const std::filesystem::path &path, ETextureFallback textureFallback);
	CModelVK *LoadModel(const std::filesystem::path &path);

	static CSceneVK *ToSceneVK(IScene *baseScene)
	{
#ifdef _DEBUG
		return dynamic_cast<CSceneVK*>(baseScene);
#else
		return static_cast<CSceneVK*>(baseScene);
#endif
	}

	CRendererVK *renderer = nullptr;

	std::vector<std::unique_ptr<CMaterialVK>> materials;
	std::unordered_map<std::string, std::unique_ptr<CModelVK>> models;
	std::unordered_map<std::string, std::unique_ptr<CTextureVK>> textures;

	CTextureVK *GetFallbackTexture(ETextureFallback fallback) const
	{
		switch (fallback)
		{
			case ETextureFallback::Error:
				return textureError;
				break;
		}

		// This should never happen
		return textureError;
	}

	// TODO: Should we really store these?
	CTextureVK *textureError = nullptr;
	CMaterialVK *materialError = nullptr;

	CModelVK *cube = nullptr;
	std::vector<std::unique_ptr<CViewVK>> views;
};