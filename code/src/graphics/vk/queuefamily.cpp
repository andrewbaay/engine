#include "queuefamily.h"
#include "renderwindow.h"

#include <vector>

QueueFamilyIndices FindQueueFamilies(CRenderWindow *renderWindow, VkPhysicalDevice device)
{
	QueueFamilyIndices indices;

	uint32_t queueFamilyCount = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);

	std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
	vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());

	for (uint32_t index = 0; index < queueFamilyCount; ++index)
	{
		if (queueFamilies[ index ].queueCount > 0)
		{
			if (queueFamilies[ index ].queueFlags & VK_QUEUE_GRAPHICS_BIT)
				indices.graphicsFamily = index;

			VkBool32 presentSupport = VK_FALSE;
			vkGetPhysicalDeviceSurfaceSupportKHR(device, static_cast< uint32_t >( index ), renderWindow->surface, &presentSupport);

			if (presentSupport == VK_TRUE)
				indices.presentFamily = index;

			if (indices.IsComplete())
				break;
		}
	}

	return indices;
}