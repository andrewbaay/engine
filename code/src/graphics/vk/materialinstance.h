#pragma once

#include <cstdint>
#include <string>
#include <unordered_map>
#include <memory>

#include "materialvk.h"
#include "texturevk.h"
#include "shaderbuffer.h"

class CMaterialInstanceVK
{
public:

	CShaderBuffer *GetBuffer(uint32_t binding) { return shaderBuffers[binding]; }

	void SetBuffer(uint32_t binding, CShaderBuffer *shaderBuffer) { shaderBuffers[binding] = shaderBuffer; }
	void SetSampler(uint32_t binding, CTextureVK *texture) { samplers[binding] = texture; }

	CMaterialVK *material = nullptr;

	VkDescriptorPool descriptorPool = VK_NULL_HANDLE;
	std::vector<VkDescriptorSet> descriptorSets;

	std::unordered_map<uint32_t, CShaderBuffer*> shaderBuffers;
	std::unordered_map<uint32_t, CTextureVK*> samplers;
};