#include "shadervk.h"
#include "shadersystem.h"
#include "renderervk_impl.h"
#include "graphicsvk.h"

#include <stdexcept>

void CShaderVK::CreateDescriptorSetLayout()
{
	CGraphicsVK *graphics = shaderSystem->renderer->graphics;
	auto bindings = GetDescriptorSetLayoutBindings();

	VkDescriptorSetLayoutCreateInfo layoutInfo = {};
	layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	layoutInfo.bindingCount = static_cast<uint32_t>(bindings.size());
	layoutInfo.pBindings = bindings.data();

	if (vkCreateDescriptorSetLayout(graphics->device, &layoutInfo, nullptr, &descriptorSetLayout) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create descriptor set layout");
}

void CShaderVK::CreateGraphicsPipelineLayout()
{
	CGraphicsVK *graphics = shaderSystem->renderer->graphics;

	VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
	pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutInfo.setLayoutCount = 1;
	pipelineLayoutInfo.pSetLayouts = &descriptorSetLayout;
	pipelineLayoutInfo.pushConstantRangeCount = 0;
	pipelineLayoutInfo.pPushConstantRanges = nullptr;

	if (vkCreatePipelineLayout(graphics->device, &pipelineLayoutInfo, nullptr, &pipelineLayout) != VK_SUCCESS)
		throw std::runtime_error("[Vulkan]Failed to create pipeline layout");
}