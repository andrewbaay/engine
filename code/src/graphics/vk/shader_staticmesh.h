#pragma once

#include "shadervk.h"

class CShaderStaticMesh : public CShaderVK
{
public:
	void InitShader() override;
	void InitMaterialInstance(CSceneVK *scene, CMaterialInstanceVK *instance) override;

	void UpdateBuffers(CMeshVK *mesh, CViewVK *view, uint32_t frame) override;

	void CreateGraphicsPipeline() override;

	std::vector<VkDescriptorSetLayoutBinding> GetDescriptorSetLayoutBindings() const override
	{
		return
		{
			{
				// Diffuse sampler
				0,											// .binding
				VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,	// .descriptorType
				1,											// .descriptorCount
				VK_SHADER_STAGE_FRAGMENT_BIT,				// .stageFlags
				nullptr										// .pImmutableSamplers
			},
			{
				// MVP Uniform buffer
				1,											// .binding
				VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,			// .descriptorType
				1,											// .descriptorCount
				VK_SHADER_STAGE_VERTEX_BIT,					// .stageFlags
				nullptr										// .pImmutableSamplers
			}
		};
	}
};