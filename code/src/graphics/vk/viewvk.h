#pragma once

#include "view.h"

class CSceneVK;

class CViewVK : public IView
{
public:
	static CViewVK *ToViewVK(IView *viewBase)
	{
#ifdef _DEBUG
		return dynamic_cast<CViewVK*>(viewBase);
#else
		return static_cast<CViewVK*>(viewBase);
#endif
	}

	void SetCamera(Camera *inCamera) override { camera = inCamera; }

	CSceneVK *scene = nullptr;
	Camera *camera = nullptr;
};