#include "console.h"

#ifdef _WIN32
#include <Windows.h>
#endif

CConsole::CConsole()
{
#ifdef _WIN32
	// This lets us have color text :D
	auto consoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);

	if (consoleHandle)
	{
		DWORD flags = {};
		GetConsoleMode(consoleHandle, &flags);
		SetConsoleMode(consoleHandle, flags | ENABLE_VIRTUAL_TERMINAL_PROCESSING);
	}
#endif
}
