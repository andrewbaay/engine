#include <iostream>
#include <thread>
#include "service.h"
#include "resourceloader.h"
#include "graphics.h"
#include "console.h"
#include "config.h"
#include "input/input.h"
#include "components/transform.h"

#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>

class CMainService : public CService<IGraphics>
{
public:
	CMainService(CServer *s) : CService(s)
	{
		window->RegisterOnClosed(std::bind(&CMainService::WindowClosed, this, std::placeholders::_1));
		renderer->SetClearColor(0.39f, 0.58f, 0.93f, 1.0f);

		input->BindKey("Forward", SDLK_w);
		input->BindKey("Back", SDLK_s);
		input->BindKey("Left", SDLK_a);
		input->BindKey("Right", SDLK_d);
		input->BindKey("LookUp", SDLK_UP);
		input->BindKey("LookDown", SDLK_DOWN);
		input->BindKey("TurnRight", SDLK_RIGHT);
		input->BindKey("TurnLeft", SDLK_LEFT);
		input->BindKey("Jump", SDLK_SPACE);
		input->BindKey("Crouch", SDLK_LCTRL);
		input->BindKey("Speed", SDLK_LSHIFT);
		input->BindKey("ZoomIn", SDLK_KP_PLUS);
		input->BindKey("ZoomOut", SDLK_KP_MINUS);
		input->BindKey("Quit", SDLK_ESCAPE);

		input->BindMouseButton("Forward", SDL_BUTTON_LEFT);
		input->BindMouseButton("Back", SDL_BUTTON_RIGHT);
	}

	void WindowClosed(IWindow *)
	{
		KillAll();
	}

	void KillAll()
	{
		scene->DeleteView(view);
		renderer->DeleteScene(scene);
		graphics->DeleteRenderer(renderer);
		graphics->DeleteWindow(window);
		renderer = nullptr;
		Halt();
	}

	void UpdateCamera()
	{
		static glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);
		static glm::vec3 forward = glm::vec3(0.0f, 0.0f, -1.0f);
		static glm::vec3 right = glm::vec3(1.0f, 0.0f, 0.0f);

		glm::quat xRot = glm::angleAxis(glm::radians(mY), glm::normalize(glm::vec3(-1.0f, 0.0f, 0.0f)));
		glm::quat yRot = glm::angleAxis(glm::radians(mX), glm::normalize(glm::vec3(0.0f, 1.0f, 0.0f)));
		transform.rotation = xRot * yRot;

		camera.viewMatrix = ToTransformation(transform);

		camera.forward = forward * yRot;
		camera.up = up;
		camera.right = right * yRot;

		camera.back = -camera.forward;
		camera.down = -camera.up;
		camera.left = -camera.right;
	}

	void InputCamera(float dt)
	{
		const float speed = input->IsPressed("Speed") ? 10.0f : 5.0f;

		if (input->IsPressed("Forward"))
			transform.position += camera.forward * speed * dt;
		else if (input->IsPressed("Back"))
			transform.position += camera.back * speed * dt;

		if (input->IsPressed("Right"))
			transform.position += camera.right * speed * dt;
		else if (input->IsPressed("Left"))
			transform.position += camera.left * speed * dt;

		if (input->IsPressed("Jump"))
			transform.position += glm::vec3(0.0f, 1.0f, 0.0f) * speed * dt;
		else if (input->IsPressed("Crouch"))
			transform.position += glm::vec3(0.0f, -1.0f, 0.0f) * speed * dt;

		if (input->IsPressed("LookUp"))
			mY += 180.0f * dt;
		else if (input->IsPressed("LookDown"))
			mY -= 180.0f *dt;

		if (input->IsPressed("TurnRight"))
			mX += 180.0f * dt;
		else if (input->IsPressed("TurnLeft"))
			mX -= 180.0f * dt;

		mX += (float)input->GetMouseDeltaX() * 0.1f;
		mY -= (float)input->GetMouseDeltaY() * 0.1f;

		auto constrain = [](float num) -> float
		{
			num = std::fmod(num, 360.0f);
			return (num < 0.0f) ? num += 360.0f : num;
		};

		mX = constrain(mX);
		mY = std::clamp(mY, -70.0f, 70.0f);

		if (input->IsJustPressed("ZoomIn"))
			transform.scale += 0.1f;
		else if (input->IsJustPressed("ZoomOut"))
			transform.scale -= 0.1f;
	}

	void Update(std::size_t dt) override
	{
		const float flDt = (float)dt / (float)1000000;

		UpdateCamera();
		InputCamera(flDt);

		if (input->IsPressed("Quit"))
			KillAll();

		if (renderer)
		{
			renderer->BeginFrame();
			renderer->DrawView(view);
			renderer->EndFrame();
		}
	}

private:
	IGraphics *graphics = Get<IGraphics>();
	IInput *input = Get<IInput>();

	IWindow *window = graphics->CreateWindow("Thermite Engine", 1280, 720, false);
	IRenderer *renderer = graphics->CreateRenderer(window);
	IScene *scene = renderer->CreateScene();
	Camera camera;
	IView *view = scene->CreateView(&camera);

	Transform transform = { glm::vec3( 0.0f, 0.0f, 3.0f ) };

	float mX = 0.0f;
	float mY = 0.0f;
};

class CSDLWrapper
{
public:
	CSDLWrapper()
	{
		if (SDL_Init(SDL_INIT_TIMER | SDL_INIT_VIDEO | SDL_INIT_GAMECONTROLLER) < 0)
			throw std::runtime_error(SDL_GetError());
	}

	~CSDLWrapper()
	{
		SDL_Quit();
	}
};

int main(int argc, char *argv[])
{
	if (argc < 2)
	{
		std::cerr << "Pass the path to thermite.cfg as an argument please.\n";
		return 1;
	}

	CSDLWrapper wrapper;
	CConsole console;
	CServer server;

	server.LoadFromConfig(argv[1]);

	CMainService mainService(&server);

	server.EventLoop();
	return 0;
}
