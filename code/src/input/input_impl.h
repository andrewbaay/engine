#pragma once

#include <unordered_map>
#include <unordered_set>

#include "input.h"
#include "graphics/graphics.h"

class CInput : public IInput, public CService<>
{
public:
	using CService<>::CService;

	int GetMouseDeltaX() const override { return mouseDeltaX; }
	int GetMouseDeltaY() const override { return mouseDeltaY; }

	bool IsPressed(const std::string &buttonName) override { return buttonMap[buttonName].isPressed; }
	bool IsReleased(const std::string &buttonName) override { return !buttonMap[buttonName].isPressed; }

	bool IsJustPressed(const std::string &buttonName) override
	{
		auto &button = buttonMap[buttonName];
		return (button.isPressed && !button.wasPressed);
	}

	bool IsJustReleased(const std::string &buttonName) override
	{
		auto &button = buttonMap[buttonName];
		return (!button.isPressed && button.wasPressed);
	}

	void BindKey(const std::string &buttonName, SDL_Keycode key) override { buttonMap[buttonName].keyBindings.insert(key); }
	void UnbindKey(const std::string &buttonName, SDL_Keycode key) override { buttonMap[buttonName].keyBindings.erase(key); }

	void BindMouseButton(const std::string &buttonName, uint8_t mouseButton) override { buttonMap[buttonName].mouseButtonBindings.insert(mouseButton); }
	void UnbindMouseButton(const std::string &buttonName, uint8_t mouseButton) override { buttonMap[buttonName].mouseButtonBindings.erase(mouseButton); }

	void Update(size_t) override;

private:

	struct Button
	{
		bool isPressed = false;
		bool wasPressed = false;
		std::unordered_set<SDL_Keycode> keyBindings;
		std::unordered_set<uint8_t> mouseButtonBindings;
	};

	std::unordered_map<std::string, Button> buttonMap = {};
	std::unordered_map<SDL_Keycode, bool> keyboardState = {};
	std::unordered_map<uint8_t, bool> mouseState = {};

	IGraphics *graphics = Get<IGraphics>();

	int mouseDeltaX = 0;
	int mouseDeltaY = 0;
};