#include "input_impl.h"
#include "log/log.h"

void CInput::Update(size_t)
{
	for (auto &[buttonName, button] : buttonMap)
	{
		button.wasPressed = button.isPressed;
		button.isPressed = false;

		for (const auto &key : button.keyBindings)
		{
			if (keyboardState[key])
			{
				button.isPressed = true;
				break;
			}
		}

		if (!button.isPressed)
		{
			// Not pressed by key, check mouse
			for (const auto &mouseButton : button.mouseButtonBindings)
			{
				if (mouseState[mouseButton])
				{
					button.isPressed = true;
					break;
				}
			}
		}
	}

	mouseDeltaX = 0;
	mouseDeltaY = 0;

	SDL_Event event = {};
	while (SDL_PollEvent(&event))
	{
		switch (event.type)
		{
			case SDL_WINDOWEVENT:
			{
				graphics->HandleWindowEvent(event.window);
				break;
			}
			case SDL_KEYDOWN:
			{
				const SDL_Keycode keyCode = event.key.keysym.sym;
				keyboardState[keyCode] = true;
				break;
			}
			case SDL_KEYUP:
			{
				const SDL_Keycode keyCode = event.key.keysym.sym;
				keyboardState[keyCode] = false;
				break;
			}
			case SDL_MOUSEBUTTONDOWN:
			{
				const uint8_t mouseButton = event.button.button;
				mouseState[mouseButton] = true;
				break;
			}
			case SDL_MOUSEBUTTONUP:
			{
				const uint8_t mouseButton = event.button.button;
				mouseState[mouseButton] = false;
				break;
			}
			case SDL_MOUSEMOTION:
			{
				mouseDeltaX += event.motion.xrel;
				mouseDeltaY += event.motion.yrel;
				break;
			}
		}
	}
}