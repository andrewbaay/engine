#include "input_impl.h"
#include "log/log.h"

extern "C" THR_EXPORT void ModuleInit(CServer::CServerInstance *serverInstance, CServer *parent)
{
	Log::Init(parent->Get<ILogger>(), "Input", fmt::color::lawn_green);
	serverInstance->MakeService<CInput>(parent);
}

extern "C" THR_EXPORT void ModuleShutdown()
{
	Log::Print("Shutting down...\n");
}