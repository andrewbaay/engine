#include "service.h"
#include "defines.h"
#include "fmt/format.h"

#include <cpptoml.h>
#include <iostream>
#include <SDL2/SDL.h>
#include <filesystem>

namespace fs = std::filesystem;

void CServer::EventLoop()
{
	while (shouldRun)
	{
		for (auto &timedService : services)
		{
			auto &[lastTime, service] = (*timedService);

			auto now = std::chrono::steady_clock::now();
			service->Update(std::chrono::duration_cast<std::chrono::microseconds>(now - lastTime).count());
			lastTime = now;
		}
	}
}

CServer::~CServer()
{
	for (auto &module : modules)
	{
		module->moduleShutdownFn();
	}
}

void CServer::Halt()
{
	shouldRun = false;
}

void CServer::LoadFromConfig(const std::string &configFile)
{
	std::shared_ptr<cpptoml::table> config = cpptoml::parse_file(configFile);

	const auto serviceList = config->get_table_array("service");

	for (const auto &svc : *serviceList)
	{
		//std::string libstr, constructor, destructor;
		fs::path libpath = SDL_GetBasePath();

		auto libstr = svc->get_as<std::string>("libpath");

		if (!libstr)
		{
			throw std::runtime_error("You need to specify a library path for this service");
		}

		libpath /= *libstr + THR_DLL_EXT;

		std::cerr << "Loading library " << libpath << std::endl;

		auto loadObject = [&libpath]() -> void*
		{
			void *object = SDL_LoadObject(libpath.string().c_str());
			if (!object)
				throw std::runtime_error(fmt::format("Failed to load library {}: {}", libpath.string(), SDL_GetError()));
			
			return object;
		};

		auto loadFunction = [](void *handle, const char *name) -> void*
		{
			void *function = SDL_LoadFunction(handle, name);
			if (!function)
				throw std::runtime_error(fmt::format("Failed to load function {}.", name));
			
			return function;
		};

		modules.insert(modules.begin(), std::make_unique<Module>());
		Module &module = *modules.front();

		module.handle = {
			loadObject(),
			SDL_UnloadObject
		};

		module.serverInstance = std::make_unique<CServer::CServerInstance>(this);
		module.moduleInitFn = (void (*)(CServer::CServerInstance*, CServer*))loadFunction(module.handle.get(), "ModuleInit");
		module.moduleShutdownFn = (void (*)())loadFunction(module.handle.get(), "ModuleShutdown");
		module.moduleName = libpath.filename().string();

		module.moduleInitFn(module.serverInstance.get(), this);
	}
}
