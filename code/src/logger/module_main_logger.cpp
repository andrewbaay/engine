#include "service.h"
#include "log/log.h"
#include "logger_impl.h"

extern "C" THR_EXPORT void ModuleInit(CServer::CServerInstance *serverInstance, CServer *parent)
{
    ILogger *logger = serverInstance->MakeService<CLogger>(parent);
	Log::Init(logger, "Logger", fmt::color::pink);
}

extern "C" THR_EXPORT void ModuleShutdown()
{
    Log::Print("Shutting down...\n");
}