#include "logger_impl.h"

#include <stdio.h>


constexpr std::string_view SeverityAsString(ESeverity severity)
{
	constexpr std::string_view severityString[3] = {
		"Info",
		"Warning",
		"Error"
	};

	return severityString[static_cast<size_t>(severity)];
}

void CLogChannel::Print(ESeverity severity, const std::string_view &message)
{
	auto formattedMsg = fmt::format("[{}] {}", channelName, message);
	if (HasOutputFile())
	{
		channelOutputFile->outputFile << fmt::format( "[ {} ] {}", SeverityAsString(severity), formattedMsg);
	}

	channelOwner->Log(this, severity, formattedMsg);
}

ILogChannel *CLogger::CreateLogChannel(const std::string_view &name)
{
	logChannels.insert(logChannels.begin(), std::make_unique<CLogChannel>(this, name));
	return logChannels.front().get();
}

ILogChannel *CLogger::CreateLogChannel(const std::string_view &name, fmt::color defaultColor)
{
	logChannels.insert(logChannels.begin(), std::make_unique<CLogChannel>(this, name, defaultColor));
	return logChannels.front().get();
}

ILogChannel *CLogger::CreateLogChannel(const std::string_view &name, const std::filesystem::path &outputFile)
{
	logChannels.insert(logChannels.begin(), std::make_unique<CLogChannel>(this, name, outputFile));
	return logChannels.front().get();
}

ILogChannel *CLogger::CreateLogChannel(const std::string_view &name, const std::filesystem::path &outputFile, fmt::color defaultColor)
{
	logChannels.insert(logChannels.begin(), std::make_unique<CLogChannel>(this, name, outputFile, defaultColor));
	return logChannels.front().get();
}

void CLogger::DestroyLogChannel(ILogChannel *logChannel)
{
	for (auto it = logChannels.begin(); it != logChannels.end(); ++it)
	{
		if (it->get() == logChannel)
		{
			logChannels.erase(it);
			return;
		}
	}
}

void CLogger::Log(ESeverity severity, std::string message)
{
	switch (severity)
	{
	case ESeverity::Info:
		fprintf(stderr, "[ INFO ] %s\n", message.c_str());
		break;
	case ESeverity::Warning:
		fprintf(stderr, "[ WARNING ] %s\n", message.c_str());
		break;
	case ESeverity::Error:
		fprintf(stderr, "[ ERROR ] %s\n", message.c_str());
	};
}

void CLogger::Log(ILogChannel *logChannel, ESeverity severity, const std::string_view &message)
{
	switch (severity)
	{
		case ESeverity::Info:
		{
			if (logChannel->HasColor())
				std::cout << fmt::format(fmt::fg(logChannel->GetColor()), "{}", message);
			else
				std::cout << message;

			break;
		}
		case ESeverity::Warning:
		{
			std::cout << fmt::format(fmt::fg(fmt::color::golden_rod), "{}", message);
			break;
		}
		case ESeverity::Error:
		{
			std::cout << fmt::format(fmt::fg(fmt::color::red), "{}", message);
			break;
		}
		default:
		{
			break;
		}
	}
}