#pragma once

#include "logger.h"

#include <fstream>
#include <optional>

class CLogChannel : public ILogChannel
{
	struct OutputFile
	{
		std::filesystem::path fileName;
		std::ofstream outputFile;
	};

public:
	CLogChannel(ILogger *logger, const std::string_view &name) : channelOwner(logger), channelName(name) {}
	CLogChannel(ILogger *logger, const std::string_view &name, fmt::color defaultColor) : channelOwner(logger), channelName(name), channelColor(defaultColor) {}
	CLogChannel(ILogger *logger, const std::string_view &name, const std::filesystem::path &outputFile) : CLogChannel(logger, name)
	{
		channelOutputFile = OutputFile{};
		channelOutputFile->fileName = outputFile;
		channelOutputFile->outputFile.open(outputFile, std::ios::app);
	}

	CLogChannel(ILogger *logger, const std::string_view &name, const std::filesystem::path &outputFile, fmt::color defaultColor) : channelOwner(logger), channelName(name), channelColor(defaultColor)
	{
		channelOutputFile = OutputFile{};
		channelOutputFile->fileName = outputFile;
		channelOutputFile->outputFile.open(outputFile, std::ios::app);
	}

	void Print(ESeverity severity, const std::string_view &message) override;

	bool HasOutputFile() const { return (channelOutputFile.has_value() && channelOutputFile->outputFile.is_open()); }
	bool HasColor() const override { return channelColor.has_value(); }

	fmt::color GetColor() const override
	{
		// Would rather give a default color than throw an exception
		return channelColor.value_or(fmt::color::white);
	}

private:
	ILogger *channelOwner = nullptr;
	std::string channelName;
	std::optional<OutputFile> channelOutputFile;
	std::optional<fmt::color> channelColor;
};

class CLogger : public ILogger, public CService<>
{
public:
	using CService<>::CService;

	ILogChannel *CreateLogChannel(const std::string_view &name) override;
	ILogChannel *CreateLogChannel(const std::string_view &name, fmt::color defaultColor) override;
	ILogChannel *CreateLogChannel(const std::string_view &name, const std::filesystem::path &outputFile) override;
	ILogChannel *CreateLogChannel(const std::string_view &name, const std::filesystem::path &outputFile, fmt::color defaultColor) override;

	void DestroyLogChannel(ILogChannel *logChannel) override;

	void SetOutput(std::size_t) override {} // unimplemented
	void SetOutput(const std::string &) override {} // unimplemented

	void Log(ESeverity severity, std::string message) override;
	void Log(ILogChannel *channel, ESeverity severity, const std::string_view &message) override;

	void Update(std::size_t) override {}

private:
	std::vector<std::unique_ptr<ILogChannel>> logChannels;
};