#include "log/log.h"
#include "renderer_impl.h"

extern "C" THR_EXPORT void ModuleInit(CServer::CServerInstance *serverInstance, CServer *parent)
{
	Log::Init(parent->Get<ILogger>(), "Renderer", fmt::color::cyan);
	serverInstance->MakeService<CRenderer>(parent, "renderer");
}

extern "C" THR_EXPORT void ModuleShutdown()
{
	Log::Print("Shutting down...\n");
}