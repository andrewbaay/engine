#pragma once

#include "renderer.h"

class CWindow : public IWindow
{
public:
	CWindow() = default;
	~CWindow() override = default;
	
	void RegisterOnClosed(std::function<void(IWindow *)> function) override { onClosed = function; }
	
	SDL_Window *handle = nullptr;
	
	std::function<void(IWindow *)> onClosed = {};
};

class CRenderer : public IRenderer, public CService<>
{
public:
	using CService<>::CService;
	
	IWindow *CreateWindow(const std::string_view &title, int width, int height) override;
	void DeleteWindow(IWindow *window) override;

	void HandleWindowEvent(const SDL_WindowEvent &windowEvent) override;
	
	void Update(std::size_t since) override;
	
private:
	std::unordered_map<uint32_t, CWindow *> windows = {};
};