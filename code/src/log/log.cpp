#include "log.h"

namespace Log
{
	namespace Internal
	{
		ILogChannel *logChannel = nullptr;
	}

	void Init(ILogger *logger, const std::string_view &name)
	{
		Internal::logChannel = logger->CreateLogChannel(name);
	}

	void Init(ILogger *logger, const std::string_view &name, fmt::color defaultColor)
	{
		Internal::logChannel = logger->CreateLogChannel(name, defaultColor);
	}

	void Init(ILogger *logger, const std::string_view &name, const std::filesystem::path &outputFile)
	{
		Internal::logChannel = logger->CreateLogChannel(name, outputFile);
	}

	void Init(ILogger *logger, const std::string_view &name, const std::filesystem::path &outputFile, fmt::color defaultColor)
	{
		Internal::logChannel = logger->CreateLogChannel(name, outputFile, defaultColor);
	}
}