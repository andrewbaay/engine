#pragma once

#include <SDL2/SDL.h>
#include "service.h"
#include "defines.h"

/**
 * @class IInput
 * @author Spirrwell
 * @date 04/25/20
 * @file input.h
 * @brief Handles SDL events and input from input devices.
 * @note This is really barebones at the moment.
 */
class IInput
{
protected:
	virtual ~IInput() = default;

public:

	/**
	 * @brief Gets the change in mouse position on the X axis
	 * @return The change in mouse position on the X axis
	*/
	virtual int GetMouseDeltaX() const = 0;

	/**
	 * @brief Gets the change in mouse position on the Y axis
	 * @return The change in mouse position on the Y axis
	*/
	virtual int GetMouseDeltaY() const = 0;

	/**
	 * @brief Checks if button is pressed.
	 * @param buttonName The name of the button to check.
	 * @return True if button is pressed.
	 */
	virtual bool IsPressed(const std::string &buttonName) = 0;

	/**
	 * @brief Checks if button is released.
	 * @param buttonName The name of the button to check.
	 * @return True if button is released.
	 */
	virtual bool IsReleased(const std::string &buttonName) = 0;


	/**
	 * @brief Checks if button was JUST pressed.
	 * @param buttonName The name of the button to check.
	 * @return True if button was JUST pressed.
	 */
	virtual bool IsJustPressed(const std::string &buttonName) = 0;

	/**
	 * @brief Checks if button was JUST released.
	 * @param buttonName The name of the button to check.
	 * @return True if button was JUST released.
	 */
	virtual bool IsJustReleased(const std::string &buttonName) = 0;

	/**
	 * @brief Binds key to a particular button.
	 * @param buttonName The name of the button to bind the key to.
	 * @param key The key we're binding to the button.
	*/
	virtual void BindKey(const std::string &buttonName, SDL_Keycode key) = 0;

	/**
	 * @brief Unbinds key from a particular button.
	 * @param buttonName The name of the button to unbind the key from.
	 * @param key The key we're unbinding from the button.
	*/
	virtual void UnbindKey(const std::string &buttonName, SDL_Keycode key) = 0;

	/**
	 * @brief Binds mouse button to a particular button.
	 * @param buttonName The name of the button to bind the mouse button to.
	 * @param mouseButton The mouse button we're binding to the button.
	*/
	virtual void BindMouseButton(const std::string &buttonName, uint8_t mouseButton) = 0;

	/**
	 * @brief Unbinds mouse button from a particular button.
	 * @param buttonName The name of the button to unbind the mouse button from.
	 * @param mouseButton The mouse button we're unbinding from the button.
	*/
	virtual void UnbindMouseButton(const std::string &buttonName, uint8_t mouseButton) = 0;
};