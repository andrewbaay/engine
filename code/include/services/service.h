#pragma once

#include <iostream>
#include <stdint.h>
#include <vector>
#include <map>
#include <chrono>
#include <type_traits>
#include <functional>
#include <string>
#include <memory>

class CServer;

/**
 * @class IService
 * @author swissChili
 * @date 30/03/20
 * @file service.h
 * @brief Abstract service class. Defines API through which updates are invoked.
 */
class IService
{
public:
	virtual ~IService() = default;
	virtual void Join(CServer *o, const std::string &id = "") = 0;
	
	/**
	 * @brief Update method called every game tick.
	 * @param since Microseconds since last update to this service.
	 */
	virtual void Update(std::size_t since) = 0;
};

/**
 * @class CServer
 * @author swissChili
 * @date 30/03/20
 * @file service.h
 * @brief Server class which manages services and runs the event loop.
 */
class CServer
{
public:
	~CServer();

	class CServerInstance
	{
	public:
		CServerInstance(CServer *server) : owner(server) {}

		CServer *GetServer() const { return owner; }

		template<typename T, typename ... Args>
		T *MakeService(Args && ...args)
		{
			services.insert(services.begin(), std::make_unique<T>(args...));
			return static_cast<T*>(services.front().get());
		}

	private:
		CServer *owner = nullptr;
		std::vector<std::unique_ptr<IService>> services;
	};

	struct Module
	{
		std::unique_ptr<void, std::function<void(void*)>> handle;
		std::unique_ptr<CServer::CServerInstance> serverInstance;
		std::function<void(CServer::CServerInstance*, CServer*)> moduleInitFn;
		std::function<void()> moduleShutdownFn;
		std::string moduleName;
	};

	/**
	 * @brief Register a service to the server.
	 * @param svc A pointer to the service.
	 */
	template <typename... T>
	void Register(IService *svc, const std::string &id)
	{
		(
			CheckService<T>(), ...
		);
		services.push_back(std::make_shared<CTimedService>(std::chrono::steady_clock::now(), svc));

		if (id != "")
		{
			idCache[id].push_back(services.back());
		}
	}

	/**
	 * @brief Reads a configuration file, loading the libraries it specifies into the server.
	 * @param configFile Path to the config file.
	 * @note See wiki for config file format.
	 * @throws libconfig::ParseException If the config file cannot be parsed.
	 * @throws libconfig::SettingNotFoundException If the config lacks a required field.
	 */
	void LoadFromConfig(const std::string &configFile);
	
	/**
	 * @brief Run the event loop indefinitely.
	 */
	void EventLoop();
	
	/**
	 * @brief Template method to get a service that fulfills a certain interface.
	 * @return A pointer to the first service that is a subclass of the template type.
	 * @throws std::runtime_error If a suitable service cannot be found.
	 */
	template <typename U>
	U *Get(const std::string &id = "")
	{
		if (id != "")
		{
			if (auto it = idCache.find(id); it != idCache.end())
			{
				auto &timedServices = it->second;
				for (auto &timedService : timedServices)
				{
					if (U *result = dynamic_cast<U *>(timedService->service))
					{
						return result;
					}
				}
			}
			throw std::runtime_error("Cannot find a suitable service with id");
		}

		for (auto &timedService : services)
		{
			auto &svc = timedService->service;
			if (U *result = dynamic_cast<U *>(svc))
			{
				return result;
			}
		}
		throw std::runtime_error("Cannot find a suitable service");
	}

	/**
	 * @brief Halt the event loop.
	 */
	void Halt();

private:
	struct CTimedService
	{
		CTimedService(std::chrono::time_point<std::chrono::steady_clock> lastTimeParam, IService *serviceParam) : lastTime(lastTimeParam), service(serviceParam) {}
		std::chrono::time_point<std::chrono::steady_clock> lastTime;
		IService *service = nullptr;
	};

	std::vector<std::unique_ptr<CServer::Module>> modules;
	
	std::vector<std::shared_ptr<CTimedService>> services;
	
	std::vector<IService *> unsorted;
	
	std::map<std::size_t, std::size_t> typeCache;

	std::map<std::string, std::vector<std::shared_ptr<CTimedService>>> idCache;

	bool shouldRun = true;
	
	template <typename T>
	bool CheckService()
	{
		if (typeCache.find(typeid(T).hash_code()) != typeCache.end())
			return true;
		
		for (std::size_t i = 0; i < unsorted.size(); i++)
		{
			if (dynamic_cast<T *>(unsorted[i]))
			{
				auto dependency = unsorted[i];
				unsorted.erase(unsorted.begin() + i);
				dependency->Join(this);
				return true;
			}
		}
		return false;
	}
};

/**
 * @class CService
 * @author swissChili
 * @date 30/03/20
 * @file service.h
 * @brief Service class that defines the API through which services communicate with the server.
 */
template <typename... T>
class CService : public IService
{
public:
	CService(CServer *o, const std::string &id = "")
	{
		Join(o, id);
	}
	
	void Join(CServer *o, const std::string &id) override
	{
		owner = o;
		owner->Register<T...>(this, id);
	}

	template <typename U>
	U *Get(const std::string &id = "")
	{
		if (!owner)
			throw std::runtime_error("No owner, cannot look up dependency.");
		return owner->Get<U>(id);
	}

	/**
	 * @brief Halt the server event loop.
	 */
	void Halt()
	{
		owner->Halt();
	}

private:
	CServer *owner = nullptr;
};
