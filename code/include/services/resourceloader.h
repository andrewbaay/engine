#pragma once

#include "service.h"
#include "resourcedelegate.h"
#include <filesystem>
#include <map>
#include <string>

/**
 * @class CResourceLoader
 * @author swissChili
 * @date 01/04/20
 * @file resourceloader.h
 * @brief Class that delegates resource loading to IResourceDelegates based on file extension.
 * @note IResourceDelegates should set their ID to ".<extension>".
 */
template <typename T>
class CResourceLoader : public CService<IResourceDelegate<T>>
{
public:
	CResourceLoader(CServer *s) : CService<IResourceDelegate<T>>(s)
	{}

	void Update(std::size_t) override
	{}

	/**
	 * @brief Delegates resource loading to a IResourceDelegate based on resource extension.
	 * @returns A pointer to the loaded resource;
	 * @throws std::runtime_error If a suitable IResourceDelegate could not be found.
	 */
	T LoadResource(const std::string &named)
	{
		std::string ext = std::filesystem::path(named).extension();
		if (extensionCache.find(ext) == extensionCache.end())
		{
			extensionCache[ext] = this->template Get<IResourceDelegate<T>>(ext);
		}
		return extensionCache[ext]->LoadResource(named);
	}

private:
	std::map<std::string, IResourceDelegate<T> *> extensionCache;
};
