#pragma once

#include "camera.h"

/**
 * @class IView
 * @author Spirrwell
 * @date 07/16/20
 * @file view.h
 * @brief View interface
 */
class IView
{
protected:
	virtual ~IView() = default;

public:
	virtual void SetCamera(Camera *camera) = 0;
};