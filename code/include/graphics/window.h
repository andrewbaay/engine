#pragma once

#include <functional>

/**
 * @class IWindow
 * @author Arthurdead
 * @date 04/04/20
 * @file graphics.h
 * @brief Window interface
 */
class IWindow
{
protected:
	/**
	 * @brief Window interface destructor do not call this use IGraphics::DeleteWindow instead.
	 */
	virtual ~IWindow() = default;
	
public:

	/**
	 * @brief Registers a function to be called when the window closes.
	 * @param function The function to be called.
	 */
	virtual void RegisterOnClosed(std::function<void(IWindow *)> function) = 0;
};