#pragma once

#include <glm/glm.hpp>

struct Camera
{
	glm::vec3 forward;
	glm::vec3 back;
	glm::vec3 up;
	glm::vec3 down;
	glm::vec3 right;
	glm::vec3 left;

	glm::mat4 viewMatrix;
};