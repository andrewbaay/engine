#pragma once

#include "view.h"
#include "camera.h"
#include "entt/entt.hpp"

/**
 * @class IScene
 * @author Spirrwell
 * @date 07/16/20
 * @file scene.h
 * @brief Scene interface
 */
class IScene
{
protected:
	virtual ~IScene() = default;

public:

	/**
	 * @brief Creates a 'view' into the scene through the specified camera.
	 * @param camera The camera we're viewing from.
	*/
	virtual IView *CreateView(Camera *camera) = 0;

	/**
	 * @brief Deletes a view.
	 * @param view The pointer to the view.
	*/
	virtual void DeleteView(IView *view) = 0;
};

class CBaseScene : public IScene
{
protected:
	virtual ~CBaseScene() = default;

public:
	entt::registry &Registry() { return registry; }

private:
	entt::registry registry;
};