#pragma once

#include "defines.h"
#include "window.h"
#include "scene.h"
#include "view.h"

/**
 * @class IRenderer
 * @author Arthurdead
 * @date 04/04/20
 * @file renderer.h
 * @brief Renderer interface
 */
class IRenderer
{
protected:
	/**
	 * @brief Renderer interface destructor do not call this use DeleteRenderer instead.
	 */
	virtual ~IRenderer() = default;

public:

	/**
	 * @brief Gets window we're rendering to.
	 * @return A pointer to the window we're rendering to.
	*/
	virtual IWindow *GetWindow() = 0;

	/**
	 * @brief Creates a scene.
	*/
	virtual IScene *CreateScene() = 0;

	/**
	 * @brief Deletes a scene.
	 * @param scene A pointer to the scene.
	*/
	virtual void DeleteScene(IScene *scene) = 0;

	/**
	 * @brief Set's the clear color value for color buffer.
	 * @param red Red value.
	 * @param green Green value.
	 * @param blue Blue vlue.
	 * @param alpha Alpha value.
	*/
	virtual void SetClearColor(float red, float green, float blue, float alpha) = 0;

	/**
	* @brief Begins the current frame to be rendered.
	*/
	virtual void BeginFrame() = 0;

	/**
	 * @brief Draws a particulare view into a scene.
	 * @param view A pointer to the view.
	*/
	virtual void DrawView(IView *view) = 0;

	/**
	* @brief Ends the current frame to be rendered.
	*/
	virtual void EndFrame() = 0;
};