#pragma once

#include <string_view>
#include <functional>
#include <unordered_map>
#include <SDL2/SDL.h>
#include "service.h"
#include "defines.h"
#include "renderer.h"
#include "window.h"
#include "scene.h"

/**
 * @class IGraphics
 * @author Arthurdead
 * @date 04/04/20
 * @file graphics.h
 * @brief Graphics interface
 */
class IGraphics
{
protected:
	/**
	 * @brief Graphics interface destructor do not call this.
	 */
	virtual ~IGraphics() = default;

public:
	/**
	 * @brief Creates a window.
	 * @param title The window title.
	 * @param width The window width.
	 * @param height The window height.
	 * @param fullscreen Enable fullscreen.
	 * @return A pointer to the newly created window.
	 */
	virtual IWindow *CreateWindow(const std::string_view &title, int width, int height, bool fullscreen) = 0;
	
	/**
	 * @brief Deletes a window.
	 * @param window A pointer to the window.
	 */
	virtual void DeleteWindow(IWindow *window) = 0;

	/**
	 * @brief Creates a renderer for the specified window.
	 * @param window The window to create a renderer for.
	*/
	virtual IRenderer *CreateRenderer(IWindow *window) = 0;

	/**
	 * @brief Deletes a renderer.
	 * @param renderer A pointer to the renderer.
	*/
	virtual void DeleteRenderer(IRenderer *renderer) = 0;

	/**
	 * @brief Handles a window event.
	 * @param windowEvent Reference to window event structure.
	*/
	virtual void HandleWindowEvent(const SDL_WindowEvent &windowEvent) = 0;
};