#pragma once

#include "service.h"
#include "defines.h"
#include "fmt/format.h"
#include "fmt/color.h"
#include <string>
#include <filesystem>

/**
 * @brief Represents logging severity levels.
 * @class Severity
 */
enum class ESeverity : uint8_t
{
	Info,
	Warning,
	Error,
};

class ILogChannel
{
public:
	virtual ~ILogChannel() = default;

	virtual void Print(ESeverity severity, const std::string_view &msg) = 0;
	virtual bool HasColor() const = 0;
	virtual fmt::color GetColor() const = 0;
};

/**
 * @class ILogger
 * @brief Handles logging.
 * @note This is currently shit, mostly a placeholder. Replace with real logger.
 */
class ILogger
{
protected:
	virtual ~ILogger() = default;

public:

	/**
	 * @brief Creates a log channel you can output messages to.
	 * @param name The name of the log channel to be logged with its output.
	 * @return A pointer to the newly created log channel.
	 */
	virtual ILogChannel *CreateLogChannel(const std::string_view &name) = 0;

	/**
	 * @brief Creates a log channel you can output messages to.
	 * @param name The name of the log channel to be logged with its output.
	 * @param defaultColor The default color you want this channel to output for ESeverity::Info.
	 * @return A pointer to the newly created log channel.
	 */
	virtual ILogChannel *CreateLogChannel(const std::string_view &name, fmt::color defaultColor) = 0;

	/**
	 * @brief Creates a log channel you can output messages to.
	 * @param name The name of the log channel to be logged with its output.
	 * @param outputFile The log file you want this channel to output to.
	 * @return A pointer to the newly created log channel.
	 */
	virtual ILogChannel *CreateLogChannel(const std::string_view &name, const std::filesystem::path &outputFile) = 0;

	/**
	 * @brief Creates a log channel you can output messages to.
	 * @param name The name of the log channel to be logged with its output.
	 * @param outputFile The log file you want this channel to output to.
	 * @param defaultColor The default color you want this channel to output for ESeverity::Info.
	 * @return A pointer to the newly created log channel.
	 */
	virtual ILogChannel *CreateLogChannel(const std::string_view &name, const std::filesystem::path &outputFile, fmt::color defaultColor) = 0;

	/**
	 * @brief Destroys a given log channel.
	 * @param logChannel The log channel you want to destroy.
	 * @note All log channels will be automatically cleaned up on shutdown.
	 */
	virtual void DestroyLogChannel(ILogChannel *logChannel) = 0;

	/**
	 * @brief Set the output to a certain file handle;
	 * @param handle The file handle.
	 * @note Probably shouldn't be size_t.
	 */
	virtual void SetOutput(std::size_t handle) = 0;

	/**
	 * @brief Sets the output to a file.
	 * @param path The path to the file.
	 */
	virtual void SetOutput(const std::string &path) = 0;
	
	/**
	 * @brief Log a string with a certain serverity to the current logger.
	 * @param message The string to log.
	 * @note No formatting as of now. This is bad, add formatting. The
	 *       only reason I haven't added it is because I couldn't think
	 *       of a clean way to do it without templates.
	 */
	virtual void Log(ESeverity severity, std::string message) = 0;

	/**
	 * @brief Log a message from a log channel with a given severity.
	 * @param logChannel The log channel we're logging from.
	 * @param severity Severity level.
	 * @param message The message to log.
	 */
	virtual void Log(ILogChannel *logChannel, ESeverity severity, const std::string_view &message) = 0;
};