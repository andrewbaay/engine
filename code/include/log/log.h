#pragma once

#include "fmt/format.h"
#include "fmt/color.h"
#include "logger/logger.h"
#include "service.h"

namespace Log
{
	namespace Internal
	{
		/**
		 * @brief The default log channel for this module to print to
		 */
		extern ILogChannel *logChannel;
	}

	/**
	 * @brief Initializes the default log channel for this module.
	 * @param logger Pointer to the logger service.
	 * @param name The name you want to be logged when printing from this module
	 */
	extern void Init(ILogger *logger, const std::string_view &name);

	/**
	 * @brief Initializes the default log channel for this module.
	 * @param logger Pointer to the logger service.
	 * @param name The name you want to be logged when printing from this module.
	 * @param defaultColor The default color you want the log to output for this module.
	 */
	extern void Init(ILogger *logger, const std::string_view &name, fmt::color defaultColor);

	/**
	 * @brief Initializes the default log channel for this module.
	 * @param logger Pointer to the logger service.
	 * @param name The name you want to be logged when printing from this module.
	 * @param outputFile The log file you want to output to when printing from this module.
	 */
	extern void Init(ILogger *logger, const std::string_view &name, const std::filesystem::path &outputFile);

	/**
	 * @brief Initializes the default log channel for this module.
	 * @param logger Pointer to the logger service.
	 * @param name The name you want to be logged when printing from this module.
	 * @param outputFile The log file you want to output to when printing from this module.
	 * @param defaultColor The default color you want the log to output for this module.
	 */
	extern void Init(ILogger *logger, const std::string_view &name, const std::filesystem::path &outputFile, fmt::color defaultColor);

	/**
	 * @brief Print to specific log channel.
	 * @param logChannel The log channel to print to.
	 * @param severity Severity level.
	 * @param message Message to print.
	 * @param args Optional arguments passed to fmtlib to format the message.
	 */
	template<typename ... Args>
	void Print(ILogChannel *logChannel, ESeverity severity, const std::string_view &message, Args &&... args)
	{
		if constexpr (sizeof...(Args) != 0)
		{
			auto formattedMsg = fmt::format(message, args...);
			logChannel->Print(severity, formattedMsg);
		}
		else
		{
			logChannel->Print(severity, message);
		}
	}

	/**
	 * @brief Print to specific log channel with default severity ESeverity::Info.
	 * @param logChannel The log channel to print to.
	 * @param message Message to print.
	 * @param args Optional arguments passed to fmtlib to format the message.
	 */
	template<typename ... Args>
	void Print(ILogChannel *logChannel, const std::string_view &message, Args &&... args)
	{
		return Print(logChannel, ESeverity::Info, message, args...);
	}

	/**
	 * @brief Print to default log channel.
	 * @param severity Severity level.
	 * @param message Message to print.
	 * @param args Optional arguments passed to fmtlib to format the message.
	 */
	template<typename ... Args>
	void Print(ESeverity severity, const std::string_view &message, Args &&... args)
	{
		return Print(Internal::logChannel, severity, message, args...);
	}

	/**
	 * @brief Print to default log channel with default severity ESeverity::Info.
	 * @param message Message to print.
	 * @param args Optional arguments passed to fmtlib to format the message.
	 */
	template<typename ... Args>
	void Print(const std::string_view &message, Args &&... args)
	{
		return Print(ESeverity::Info, message, args...);
	}
}