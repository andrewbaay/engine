#version 460

layout( location = 0 ) out vec4 outFragColor;


layout ( location = 0 ) in vec2 inFragTexCoord;
layout ( location = 1 ) in vec4 inFragColor; // Vertex color
layout ( location = 2 ) in vec3 inFragPos;

layout( binding = 0 ) uniform sampler2D diffuseSampler;

void main()
{
	// Base color sampled from diffuse texture
	vec3 albedo = vec3( texture( diffuseSampler, inFragTexCoord ) );
	outFragColor = vec4( albedo, 1.0 );
}