SET glslValidator=%VULKAN_SDK%\Bin\glslangValidator.exe

%glslValidator% -V static_mesh.vert -o ../../shaders/vk/static_mesh.vert.spv
%glslValidator% -V static_mesh.frag -o ../../shaders/vk/static_mesh.frag.spv