#version 460

layout ( binding = 1 ) uniform UniformBufferObject
{
	mat4 mvp;
	mat4 model;
} MVPModel;

// UNUSED
layout ( location = 1 ) in vec3 inNormal;
layout ( location = 2 ) in vec3 inTangent;
layout ( location = 3 ) in vec3 inBitangent;
layout ( location = 6 ) in uvec4 inBoneIndex;
layout ( location = 7 ) in vec4 inBoneWeight;

layout ( location = 0 ) in vec3 inPosition;
layout ( location = 4 ) in vec2 inTexCoord;
layout ( location = 5 ) in vec4 inColor;

layout ( location = 0 ) out vec2 outfragTexCoord;
layout ( location = 1 ) out vec4 outfragColor;
layout ( location = 2 ) out vec3 outfragPos;

void main()
{
	gl_Position = MVPModel.mvp * vec4( inPosition, 1.0 );
	
	outfragColor = inColor;
	outfragTexCoord = inTexCoord;
	outfragPos = vec3( MVPModel.model * vec4( inPosition, 1.0 ) );
}
