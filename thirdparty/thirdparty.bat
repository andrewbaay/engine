@echo off
rem I'm not a batch master so feel free to improve this file

IF NOT DEFINED VSCMD_VER (
	echo Please run this file in the Developer Command Prompt for Visual Studio
	pause
	EXIT /B
)

rem Check for 7za
WHERE 7za >nul 2>nul
IF %ERRORLEVEL% NEQ 0 (
	echo Failed! Could not find 7za! Please download the standalone console version of 7-Zip from https://www.7-zip.org/download.html
	pause
	EXIT /B
)

rem Check for CMake
WHERE cmake >nul 2>nul
IF %ERRORLEVEL% NEQ 0 (
	echo Failed! Count not find CMake! Please download and install CMake from https://cmake.org/download/
	pause
	EXIT /B
)

echo Building thirdparty libraries...

:ASSIMP
IF EXIST ..\game\assimp-vc142-mt.dll GOTO SDL2
robocopy dist\bin\release ..\game *.dll
robocopy dist\bin\debug ..\game *.dll
robocopy dist\bin\debug ..\game *.pdb

:SDL2
IF EXIST SDL2-2.0.12 GOTO ULTRALIGHT
echo Downloading SDL2
call download-unzip.bat https://www.libsdl.org/release SDL2-devel-2.0.12-VC.zip .
robocopy SDL2-2.0.12\lib\x64 ..\game *.dll
robocopy SDL2-2.0.12\lib\x64 dist\bin *.dll
robocopy SDL2-2.0.12\lib\x64 dist\lib *.lib
robocopy SDL2-2.0.12\include dist\include\SDL2

:ULTRALIGHT
IF EXIST ultralight GOTO STEAMAUDIO
echo Downloading Ultralight
call download-unzip.bat https://github.com/ultralight-ux/Ultralight/releases/download/v1.1.0 ultralight-sdk-1.1.0-win-x64.7z ultralight
robocopy ultralight\bin dist\bin /S
robocopy ultralight\lib dist\lib /S
robocopy ultralight\include dist\include\Ultralight /S

:STEAMAUDIO
IF EXIST steamaudio_api GOTO DONE
echo Downloading Steam Audio
call download-unzip.bat https://github.com/ValveSoftware/steam-audio/releases/download/v2.0-beta.17 steamaudio_api_2.0-beta.17.zip .
robocopy steamaudio_api\bin\Windows\x64 dist\bin /S
robocopy steamaudio_api\lib\Windows\x64 dist\lib /S
robocopy steamaudio_api\include dist\include\steamaudio /S


:DONE
