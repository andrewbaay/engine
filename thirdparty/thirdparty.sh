#!/bin/bash

if [ ! -z ${CI+x} ]; then 
	WGET_ARGS="--show-progress"
fi

function download_steamaudio
{
	filename="steamaudio_api_${1}.zip"
	folder="${filename%.*}"

	if [[ ! -d "${folder}" ]]; then
		wget -q $WGET_ARGS "https://github.com/ValveSoftware/steam-audio/releases/download/v${1}/${filename}"
		unzip -q "${filename}" -d "${folder}"
		mv ${folder}/steamaudio_api/* "${folder}"
		rm -f -d "${folder}/steamaudio_api"
		rm -f "${filename}"
	fi
}

function download_ultralight
{
	filename="ultralight-sdk-${1}-linux-x64.7z"
	folder="${filename%.*}"

	if [[ ! -d "${folder}" ]]; then
		wget -q $WGET_ARGS "https://github.com/ultralight-ux/Ultralight/releases/download/v${1}/${filename}"
		7z x -y "${filename}" -o"${folder}" > /dev/null
		rm -f "${filename}"
	fi
}

function download_vulkan
{
	filename="vulkansdk-linux-x86_64-${1}.tar.gz"
	folder="${filename%.*}"
	
	if [[ ! -d "${folder}" ]]; then
		wget -q $WGET_ARGS "https://vulkan.lunarg.com/sdk/download/${1}/linux/vulkansdk-linux-x86_64-${1}.tar.gz"
		tar -xzf "${filename}"
		mv "${1}" "vulkansdk-linux-x86_64-${1}"
		rm -f "${filename}"
	fi
}

function git_clone
{
	if [[ ! -d "${2}" ]]; then
		git clone --recursive "https://github.com/${1}/${2}.git"
	else
		cd "${2}"
		git fetch
		git reset --quiet
		git clean -f
		cd ".."
	fi
}

# if you haven't cloned submodules:
git submodule update --init --recursive

download_ultralight "1.1.0"
download_steamaudio "2.0-beta.17"
download_vulkan "1.2.141.2"
